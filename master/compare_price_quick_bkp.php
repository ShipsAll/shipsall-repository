<?php
//Project Name : Ouiship
//Developer : Prashant Gorvadia
//Page for entering sender,recipeient,package info,shipping choise,pickup service this page work after login successfully you can get carrier shipping rates and select rates 
include('config.php');
include('header.php');
?>

<!-- Javascript start here below code for accordian functionality of bar and fetch autosuggestion of zipcode : start -->
<script src="<?php echo JS_PATH; ?>typeahead.min.js"></script>

<script type="text/javascript">

$(document).ready(function(){
$('#click').click(function()
{
$("#panel").animate({width:'toggle'},500);       
});

$("#zip").typeahead({
name : 'zip',
remote: {
url : 'statedata_quick.php?query=%QUERY'
}

});


<?php if($fromres['fname']!='' && $fromres['lname']!='' && $fromres['street1']!='' && $fromres['city']!='' && $from['state']!='' && $fromres['zip']!='' && $fromres['phone']!='' && $_SESSION['fetchuserregistration']=='' )  { 
?>
accordingformsubmitfrom();

<?php
$_SESSION['fetchuserregistration']='1';
} 
else if( $_POST['fname']!='' && $_POST['tofname']=='' && $_POST['height']=='' && $_POST['pickup_request']=='')  { 
?>
open_accordion_section2();
jQuery("#tofname").focus();

<?php
} 

if( $_SESSION['fname']!='' && $_SESSION['lname']!='' && $_SESSION['street1']!=''  && $_SESSION['city']!=''  && $_SESSION['state']!=''  && $_SESSION['zip']!=''  && $_SESSION['phone']!='' &&  $_SESSION['tofname']=='')  { 
?>
open_accordion_section2();
jQuery("#tofname").focus();

<?php
} 

else if($_POST['tofname']!='' && $_SESSION['fname']!='' && $_SESSION['height']==''  && $_SESSION['pickup_request']=='')
{
?>
open_accordion_section3();

jQuery("#height").focus();

<?php
}
else if($_SESSION['service']=='' && $_SESSION['fname']!='' && $_SESSION['tofname']!='' && $_SESSION['height']!='' )
{ 
?>
open_accordion_section4();

<?php
} 
else if($_SESSION['fname']!='' && $_SESSION['tofname']!='' && $_SESSION['height']!='' && $_SESSION['rate']!='' && $_SESSION['pickup_request']=='' )
{  
?>
open_accordion_section5();
pickupfrombarvaluefetch();


jQuery("#pickup_request").focus();
<?php
} 
else if($_SESSION['fname']!='' && $_SESSION['tofname']!='' && $_SESSION['height']!='' && $_SESSION['rate']!='' && $_SESSION['pickup_request']!='')
{  
?>
open_accordion_section5();
pickupfrombarvaluefetch();

jQuery("#pickup_request").focus();


<?php 
} 
else if($_SESSION['tofname']=='' && $_SESSION['fname']=='' && $_SESSION['height']=='' && $_SESSION['pickup_request']=='')
{  
?>
open_accordion_section1();

jQuery("#fname").focus();


<?php 
}
else if($_SESSION['fname']!='' && $_SESSION['tofname']!='' && $_SESSION['height']!='' && $_SESSION['rate']!='' && $_SESSION['pickup_request']!='' )
{  
?>
open_accordion_section5();

jQuery("#fname").focus();


<?php 
}
?>
$("#phone").focusout(function(){
close_accordion_section();
accordingformsubmitfrom();
open_accordion_section2();

}); 
$("#tophone").focusout(function(){
close_accordion_section();
accordingformsubmitto();
open_accordion_section3();

}); 
$("#weight_oz").focusout(function(){
accordingformsubmit();
}); 


}); 


function clearformdata(){
document.getElementById("shippingcalc").reset();
}

function close_accordion_section() {
$('.accordion .accordion-section-title').removeClass('active');
$('.accordion .accordion-section-content').slideUp(300).removeClass('open');
}
function open_accordion_section1(){
jQuery('#accordion-1').slideDown(300).addClass('open');
}
function open_accordion_section2() {
jQuery('#accordion-2').slideDown(300).addClass('open'); 
}
function open_accordion_section3() {
jQuery('#accordion-3').slideDown(300).addClass('open'); 
}

function open_accordion_section4(){
jQuery('#accordion-4').slideDown(300).addClass('open');
}
function open_accordion_section5(){
jQuery('#accordion-5').slideDown(300).addClass('open');
}	

function accordingformsubmitfrom(){
document.getElementById("fromcalc").submit();
jQuery('#accordion-2').slideDown(300).addClass('open');
}	
function accordingformsubmitto(){
document.getElementById("tocalc").submit();
jQuery('#accordion-3').slideDown(300).addClass('open');
}	
function accordingformsubmit(){
document.getElementById("shippingcalc").submit();
}
function pricefreightformsubmit(){
document.getElementById("pricefreightform").submit();
}

function pickuprequestformsubmit(){
document.getElementById("pickuprequestmiscform").submit();
}


</script>
<!-- below code for accordian functionality of bar : end -->
<body>
<!-- topbar starts -->
<?php include('header-menu.php'); ?>
<!-- topbar ends -->
<div class="ch-container" style="text-align:center;">
<div>
<h3><a href="" title="" data-toggle="tooltip" style="text-transform: capitalize;text-align:left !important;" target="_blank" data-original-title="Click here to see Help">
<div class="toggle"></div>
</a> COMPARE PRICES</h3>
</div>
<div class="content">
<h5 style="line-height: 5px;">Please enter <b>Senders</b> and <b>Recipients s</b>, as well as your <b>Package Details</b><br>
<br>
</h5>
<h5 style="line-height: 10px;"> (dimensions and weight).After doing so you will can select your <b>Shipping</b> choice. All other fields are optional.<br>
<br>
</h5>
<h5 style="line-height: 10px;">When all required information is entered, click on <b>Submit</b> to see a Preview of your order.<br>
<br>
</h5>
<h5>Help is always available by clicking on the <img src="<?php echo SITE_URL; ?>images/help2.png"/></h5>
</div>
<?php if(isset($_POST['weight']) || $_SESSION['weight']) { ?>
<div id="overlay" style="margin-right: 45%;margin-left: 45%; "> <img src="images/ajax-loader-7.gif" title="Shipping Rates Fetching"><strong></strong> </div>
<?php } ?>
</div>
<div class="ch-container" style="margin-left:17%;">
<div class="box col-md-10"> <br>
<div class="main">
<div class="accordion">
<form name="fromcalc" id="fromcalc" action="compare_price_quick.php" method="post">
<input type="hidden" name="frombar" value="1"/>
<input type="hidden" name="shippingsave" value="1"/>
<input type="hidden" name="fromdata" value="1"/>
<br>
<div class="accordion-section" id="accordion-section1"> <a class="accordion-section-title bartextcolor" href="#accordion-1">
<div class="bartextalignmain accordinbarshadow">
<div class="bartextalign"> Sender:
<button type="button" class="btn btn-default popover-content" id="sender_popup" data-container="body" data-title="Sender Help" data-toggle="popover" data-placement="left" data-content="Please enter Senders Name, Address and Contact Information for Tracking and Notification purposes. &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
In order to save time entering your address information, once you enter the zip code, you can select the associated city and it will fill in the City and State automatically.     &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
When you reach the end of the form you can use TAB or Submit to save your results and move to the next section. If you have entered all information correctly, you will see a summary in the Bar. If you do not see any text in the bar this means you have not entered all required fields. To resolve this issue, please click on the Bar and submit missing data and save"  style="background: transparent;border: 0px;margin-left: 42%;"> <img src="<?php echo SITE_URL; ?>images/help2.png"/> </button>
</div>
<div class="bartextalign2"> <font color="white">
<?php  if($_SESSION['zip']!='' ){ echo $_SESSION['zip']; } ?>
</font> </div>
</div>
</a>
<input type="hidden" name="fromid" value="1" />
<div id="accordion-1" class="accordion-section-content accordinbarshadow">
<div class="box-content">

<div class="form-group" id="packed_shop29">


<?php if($_SESSION['city']!='' && $_SESSION['state'] && $_SESSION['zip']) { ?>
<div class="form-group" id="packed_shop1">
<input type="text" name="zip" class=" text-input form-control  textareashadow" id="zip"  value="<?php echo isset($_SESSION['combinecsz'])?$_SESSION['combinecsz']:$zipcodefromsave; ?>"  placeholder="Enter Zip Code and then select City/State from Menu*" data-provide="typeahead" autocomplete="false" required>
</div>
<?php } else { ?>
<div class="form-group" id="packed_shop1">
<input type="text" name="zip" class=" text-input form-control  textareashadow" id="zip"  value="<?php echo isset($zipcodefromsave)?$zipcodefromsave:''; ?>"  placeholder="Enter Zip Code and then select City/State from Menu*" data-provide="typeahead" autocomplete="false" required>
</div>
<?php } ?>

<div class="accordinbarshadow accordion-section-title my_btn_bg accordinbarshadowbottomsubmit">
<div class="bartextalign"  style="border-radius:80px 0 0 0;">
<input type="submit" name="fromsubmit" value="Submit"  onClick="accordingformsubmitfrom()">
</div>
</div>
</div>
</div>

<!--end .accordion-section-content--> 
<!--end .accordion-section--> 

</div>
</form>
<form name="tocalc" id="tocalc" action="compare_price_quick.php" method="post">
<input type="hidden" name="tobar" value="1"/>
<?php if($_SESSION['userid']!=''){ ?>
<input type="hidden" name="shippingsesssave" value="1"/>
<?php } else { ?>
<input type="hidden" name="shippingsave" value="1"/>
<?php } ?>
<input type="hidden" name="todata" value="1"/>
<div class="accordion-section" id="accordion-section1"> <a class="accordion-section-title bartextcolor" href="#accordion-2">
<div class="bartextalignmain accordinbarshadow">
<div class="bartextalign"> Recipient:
<button type="button" class="btn btn-default popover-content" id="rec_popup" data-container="body" data-title="Recipient Help" data-toggle="popover" data-placement="left" data-content="Please enter Recipients Name, Address and Contact Information for Tracking and Notification purposes. &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;    &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;                                 
In order to save time entering your address information, once you enter the zip code, you can select the associated city and it will fill in the City and State automatically. &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;    &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;              
When you reach the end of the form you can use TAB or Submit to save your results and move to the next section. If you have entered all information correctly, you will see a summary in the Bar. If you do not see any text in the bar this means you have not entered all required fields. To resolve this issue, please click on the Bar and submit missing data and save."  style="background: transparent;border: 0px;    margin-left: 33%;"> <img src="<?php echo SITE_URL; ?>images/help2.png"/> </button>
</div>
<div class="bartextalign2"><font color="white">
<?php  if($_SESSION['tozip']!='' ){ echo $_SESSION['tozip']; } ?>
</font></div>
</div>
</a>
<div id="accordion-2" class="accordion-section-content accordinbarshadow">
<div class="box-content">

<div class="form-group" id="packed_shop29">

<?php /*?> <?php if($_SESSION['userid']!=''){ ?>
<div class="form-group" id="packed_shop29">
<input type="text" name="tolookup" class=" text-input form-control textareashadow" id="tolookup" value ="<?php echo isset($_SESSION['tolookup'])?$_SESSION['tolookup']:''; ?>" placeholder="Enter FirstName and then select Saved Information from Menu*" required>
</div>
<?php } ?><?php */?>

<div class="form-group" id="packed_shop1">
<input type="text" name="tozip" class=" text-input form-control  textareashadow" id="tozip"  value="<?php echo isset($_SESSION['tocombinecsz'])?$_SESSION['tocombinecsz']:''; ?>"  placeholder="Enter Zip Code and then select City/State from Menu*"  data-provide="typeahead" autocomplete="off" required>
</div>

<div class="accordinbarshadow accordion-section-title my_btn_bg accordinbarshadowbottomsubmit">
<div class="bartextalign"  style="border-radius:80px 0 0 0;">
<input type="submit" name="tosubmit" class="" value="Submit"  onClick="accordingformsubmitto()">
</div>
</div>
</div>
</div>
<!--end .accordion-section-content--> 
</div>
<!--end .accordion-section-->
</form>
<form name="shippingcalc" id="shippingcalc" action="compare_price.php" method="post">
<input type="hidden" name="packagebar" value="1"/>
<?php if($_SESSION['userid']!=''){ ?>
<input type="hidden" name="shippingsesssave" value="1"/>
<?php } else { ?>
<input type="hidden" name="shippingsave" value="1"/>
<?php } ?>
<input type="hidden" name="packagedata" value="1"/>
<div class="accordion-section" id="accordion-section1"> <a class="accordion-section-title bartextcolor" href="#accordion-3" >
<div class="bartextalignmain accordinbarshadow">
<div class="bartextalign">Package Details:
<button type="button" class="btn btn-default popover-content" id="pkg_popup"  data-title="Package Details Help" data-container="body" data-toggle="popover" data-placement="left" data-content="Here you will enter your packages dimensions (length, height, width in inches) as well as the package weight.&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;    &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;         
Optional Services offers you the ability to customize your shipment. Here is an explanation of these optional services:&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;    &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;          
Saturday Delivery – Do you require that the package be delivered on a Saturday? There are shipping services available Wed – Fri which can get your package to you on Saturday if necessary.&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;    &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;         &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;   &nbsp;&nbsp; &nbsp;&nbsp;        
Ship Green – This is our Carbon Neutral fund which we use to minimize our /your carbon fingerprint.&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;    &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;  &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;        
Insurance – We insure your package up to $100. If your package is worth more than $100 please select this option and enter the value of your shipment. When you Preview your order, it will show you the associated cost so you can change it if you wish.&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;    &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;         
Special Handling Message – If you select this option your shipping label will include a message to your Carrier such as 'Please Handle with Care!' Enter whatever message you wish the Carrier to see in the form.
Delivery Confirmation – If you select this option we will notify you when your package has been delivered. This option is enabled by default and will use your e-mail as well as text address to send you notification of delivery.
After submitting your package information and any optional services click Submit and you will be presented with a selection of shipping services from which to select. You will be able to sort by price as well as arrival date.
"  style="background: transparent;border: 0px;margin-left: 2%;"> <img src="<?php echo SITE_URL; ?>images/help2.png"/> </button>
</div>
<div class="bartextalign2"></div>
</div>
</a>
<div id="accordion-3" class="accordion-section-content accordinbarshadow" style="padding:0px;">
<div style="background: #585858; display: inline-flex;width:100%;padding: 23px;">


<div class="form-group packagespacingsec"  id="packed_shop29" style="margin-left: 2%;"> Weight </div>
<div style="margin-left: 2%;">
<div class="form-group" id="packed_shop27">
<input type="number" name="weight" id="weight" class=" text-input form-control  textareashadow" value="<?php echo isset($_SESSION['weight'])?$_SESSION['weight']:''; ?>" placeholder="Weight (Pounds)*" required/>
</div>
<div class="form-group" id="packed_shop27">
<input type="number" name="weight_oz" id="weight_oz" class=" text-input form-control  textareashadow" value="<?php echo isset($_SESSION['weight_oz'])?$_SESSION['weight_oz']:''; ?>" placeholder="Weight (Ounces)*" required/>
</div>
<div class="accordinbarshadow accordion-section-title my_btn_bg pkgaccordinbarshadowbottomsubmit">
<div class="bartextalign"  style="border-radius:80px 0 0 0;">
<input type="submit" name="package_form" class="" value="Submit"  onClick="pricefreightformsubmit()">
</div>
</div>
</div>
</div>
<!--end .accordion-section-content--> 
</div>
<!--end .accordion-section-->
</form>
<div class="accordion-section" style="border-bottom:0px;"> <a class="accordion-section-title bartextcolor" href="#accordion-4">
<div class="bartextalignmain accordinbarshadow">
<div class="bartextalign" style="width:30% !important">Shipping choices
<button type="button" class="btn btn-default popover-content"  id="shipping_popup" data-title="Shipping Costs Help"  data-container="body" data-toggle="popover" data-placement="left" data-content="After you submit your Sender, Recipient, and Package Details to the application, it will calculate all available shipping services and costs available for you to use to ship. Use the Select Freight checkbox to select your freight option. You can sort by Price, Arrival Date as well as Carrier, thus making it easy to find the perfect freight choice. &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;    &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;
Once selected you will see your choice in the Shipping Costs Bar."  style="background: transparent;border: 0px;    margin-left: 13%;"><img src="<?php echo SITE_URL; ?>images/help2.png"/></button>
</div>
<div class="bartextalign2"> <font color="white">
<?php if($_SESSION['carrier']!='')
{ 
echo $_SESSION['carrier']." - $".$_SESSION['rate']." - <span style='text-transform:capitalize !important'>".$_SESSION['service']."</span> - ".$_SESSION['delivery_date_white'];
} ?>
</font></div>
</div>
</a>
<div id="accordion-4" class="accordion-section-content accordinbarshadow  accordion-section-content_sub" >
<?php 
$todaydate =  date('l', strtotime(date('y-m-d')));


try {

$today_date_find =  date("l");
if($today_date_find == 'Saturday'){
$week_start_date = date("m/d/Y", strtotime("+ 2 day"));	
}
else if($today_date_find == 'Sunday')
{
$week_start_date = date("m/d/Y", strtotime("+ 1 day"));	

}
else{
$week_start_date = date('m/d/Y',time());
}
$_SESSION['week_start_date'] = $week_start_date;

?>
<div class="box-content"><b>The below results represent Packages/Envelopes that ship by COB (<?php echo $week_start_date; ?>)</b><br>
<br>
<?php       
if($_POST['zip']!=''){      
$addressinfosender = selectquickcitystatezip($_POST['zip']);
$addressinforecepient = selectquickcitystatezip($_POST['tozip'])

// require_once("../vendor/autoload.php");
require_once("lib/easypost.php");
require_once("datecalculator.php");
$fetchapikey = getapikey();  
\EasyPost\EasyPost::setApiKey($fetchapikey);
// create addresses  LDNaaanQTfoWnVXBIDd8pw
//  print_r($_POST);exit;`
$sessweightoz =	$_POST['weight_oz']/16;
$sessionweight = $_POST['weight']*16;
$sessweight = $sessionweight + $sessweightoz;
$to_address_params = array("name"    => "Curtis",
"street1" => "6111 SW 55th Drive",
"street2" => "",
"city"    => $addressinfosender['City'],
"state"   => $addressinfosender['State'],
"zip"     => $addressinfosender['ZipCode'],
"phone"     => "9687436776");
$to_address = \EasyPost\Address::create($to_address_params);

$from_address_params = array("name"    => "Prashant",
"street1" => "2616 NE 72nd Street",
"street2" => "",
"city"    => $addressinforecepient['City'],
"state"   => $addressinforecepient['State'],
"zip"     => $addressinforecepient['ZipCode'],
"phone"     => "9687436776");

$from_address = \EasyPost\Address::create($from_address_params);

// create parcel
$parcel_params = array("length"             => 12,
"width"              => 12,
"height"             => 6,
"predefined_package" => null,
"weight"             => $sessweight			
);
$parcel = \EasyPost\Parcel::create($parcel_params);	





$shipment_params = array("from_address" => $from_address,
"to_address"   => $to_address,
"parcel"       => $parcel
);
$shipment = \EasyPost\Shipment::create($shipment_params);


}
//	$_SESSION['shippment_rate'] = '';
//			unlink($_SESSION['shippment_rate']);
//			$_SESSION['shippment_rate'] = $shipment['rates'];
//			$_SESSION['myweight'] = $sessweight;
//echo "<pre>";
//            print_r($shipment);
//            echo "</pre>";exit;
//echo "shipmentid".$shipment['id'];
//$shipment['rates']

if (count($shipment->rates) === 0) {
$shipment->get_rates();	
}

// retrieve one rate
$rate = \EasyPost\Rate::retrieve($shipment->lowest_rate());

$shipment->lowest_rate(); 
// create and verify at the same time
$verified_on_create = \EasyPost\Address::create_and_verify($from_address_params);
$verified_on_create = \EasyPost\Address::create_and_verify($to_address_params);
//echo $e->getMessage();
//for address verificaton : end

$fname = isset($shipfromrow['fname'])?$shipfromrow['fname']:$_POST['fname'];
$lname = isset($shipfromrow['lname'])?$shipfromrow['lname']:$_POST['lname'];
$street1 = isset($shipfromrow['street1'])?$shipfromrow['street1']:$_POST['street1'];
$street2 = isset($shipfromrow['street2'])?$shipfromrow['street2']:$_POST['street2'];
$city = isset($shipfromrow['city'])?$shipfromrow['city']:$_POST['city'];
$state = isset($shipfromrow['state'])?$shipfromrow['state']:$_POST['state'];
$zip = isset($shipfromrow['zip'])?$shipfromrow['zip']:$_POST['zip'];
//echo "<pre>";print_r($shipment);echo "</pre>";
//			exit;
if(count($shipment['rates']) > 0){
echo '  <table id="shiptabex" class="table table-striped table-bordered bootstrap-datatable responsive">
<thead>
<tr>
<th style="text-align:center;">Carrier  <i class="fa fa-fw fa-sort"></i></th>
<th style="text-align:center;">Rate $ <i class="fa fa-fw fa-sort"></i></th>
<th style="text-align:center;">Service <i class="fa fa-fw fa-sort"></i></th>
<th style="text-align:center;width: 13%;">Arrival <i class="fa fa-fw fa-sort"></i></th>
<th style="text-align:center;width: 1%;">Pickup Available</th>
<th style="text-align:center;width: 1%;">Select Freight</th>
</tr>
</thead>
<tbody>';
if($pickup->confirmation!=''){
echo '<tr>'.$pickup->confirmation.'</tr>';
}


for($i=0;$i<count($shipment['rates']);$i++){	
$shipment_id =  $shipment['rates'][$i]['shipment_id'];
$carrier =  $shipment['rates'][$i]['carrier'];
$rate =  $shipment['rates'][$i]['rate'];
$service =  $shipment['rates'][$i]['service'];
$currency =  $shipment['rates'][$i]['currency'];
if($shipment['rates'][$i]['delivery_days']!=''){
$delivery_days =  $shipment['rates'][$i]['delivery_days'];
$delivery_date_guaranteed =  $shipment['rates'][$i]['delivery_date_guaranteed'];
$increaseday = "+".$delivery_days."day";
// $delivery_date = date("m/d/y",strtotime($shipment['rates'][$i]['delivery_date']));
//  $delivery_date =  date('m/d/y', strtotime(date("Y/m/d") . $increaseday));
$businessdays = 1;
$holidays = array("2015-01-25");				

require_once("datecalculator.php");
$holidays = [new \DateTime('2000-12-31'), new \DateTime('2001-01-01')];
$freeDays = [new \DateTime('2000-12-28')];
if(isset($_SESSION['saturdaydelivery']) &&  $_SESSION['saturdaydelivery']=='Yes'){
$freeWeekDays = [Calculator::SUNDAY];
$result = $calculator->getDate();            // \DateTime

$calculator = new Calculator();
$calculator->setStartDate(new \DateTime(date('Y-m-d')));
$calculator->setFreeWeekDays($freeWeekDays); // repeat every week
$calculator->setHolidays($holidays);         // repeat every year
$calculator->setFreeDays($freeDays);         // don't repeat


$calculator->addBusinessDays($delivery_days);             // add X working days



echo $tomorrow_date = date("Y-m-d", strtotime("+".$delivery_days." day"));	exit;
$saturday_Date_Count =  date("l",strtotime($tomorrow_date));
if($saturday_Date_Count == 'Saturday'){
$delivery_date = $result->format('YY-m-d');
}
else
{
$delivery_date = '1';
}
}
else
{
$freeWeekDays = [Calculator::SATURDAY,Calculator::SUNDAY];
$calculator = new Calculator();
$today_date_find =  date("l");
if($today_date_find == 'Saturday'){
$week_start_date = date("Y-m-d", strtotime("+ 2 day"));	
}
else if($today_date_find == 'Sunday')
{
$week_start_date = date("Y-m-d", strtotime("+ 1 day"));	

}
else
{
$week_start_date = date('Y/m/d');
}

$calculator->setStartDate(new \DateTime($week_start_date));
$calculator->setFreeWeekDays($freeWeekDays); // repeat every week
$calculator->setHolidays($holidays);         // repeat every year
$calculator->setFreeDays($freeDays);         // don't repeat

$calculator->addBusinessDays($delivery_days);             // add X working days



$result = $calculator->getDate();            // \DateTime
$delivery_date = '<font color="#8B0000" style="font-weight:bold">'.$result->format('m/d/Y').'</font>';
$delivery_date_white = '<font color="white">'.$result->format('m/d/Y').'</font>';
}


//$delivery_date = add_business_days(date('Y-m-d'),$businessdays,$holidays,"Y-m-d",$increaseday);
}
else
{
$delivery_date = '3-7 days';
}

if($shipment['rates'][$i]['delivery_date_guaranteed']=='1' && $shipment['rates'][$i]['delivery_date_guaranteed']!='')
{
$delivery_date_guaranteed =  $delivery_date_guaranteed." Day";

}
else if($shipment['rates'][$i]['delivery_date']=='' && $shipment['rates'][$i]['delivery_date_guaranteed']=='')
{
// if($shipment['rates'][$i]['delivery_days']=='1'){
//					  $delivery_date_guaranteed =  $shipment['rates'][$i]['delivery_days']." Day";
//				   }
//				   else if($shipment['rates'][$i]['delivery_days']!='' && $shipment['rates'][$i]['delivery_days']!='1')
//				   {
//					   $delivery_date_guaranteed =  $shipment['rates'][$i]['delivery_days']." Days";
//				   }
//				   else
//				   {
$delivery_date_guaranteed = '3-7 days';
// }
}
else
{
$delivery_date_guaranteed = '';
}

if($shipment['rates'][$i]['service'] == 'Priority'){
$delivery_date = '<a href="https://www.usps.com/priority-mail/map/" class="prioritymainclr">2-3 Days</a>';
}

$rateservice = str_replace('_','&nbsp;',$shipment['rates'][$i]['service']);
$rateservice2 = str_replace('FEDEX','',$rateservice);
$rateservice3 = preg_replace('/([a-z])([A-Z])/s','$1 $2', $rateservice2); 
$rateservicefinaltext = ucwords(strtolower($rateservice3));
$bubble_title = 'FedEx First Overnight® provides delivery by 8 a.m';
$bubble_url = '';

if($shipment['rates'][$i]['service'] == 'FIRST_OVERNIGHT')
{
$bubble_title = 'FedEx First Overnight® provides delivery by 8 a.m';
$bubble_url = 'http://www.fedex.com/us/fedex/shippingservices/package/first_overnight.html';
}
else if($shipment['rates'][$i]['service'] == 'PRIORITY_OVERNIGHT')
{

$bubble_title = 'With FedEx Priority Overnight, your shipment will arrive by 10:30 a.m. to most U.S. addresses';
$bubble_url =  'http://www.fedex.com/us/fedex/shippingservices/package/standard_overnight.html';
}
else if($shipment['rates'][$i]['service'] == 'STANDARD_OVERNIGHT')
{
$bubble_title = 'Choose FedEx Standard Overnight for delivery by 3 p.m. to most addresses, and by 8 p.m. to residences';
$bubble_url =  'http://www.fedex.com/us/fedex/shippingservices/package/standard_overnight.html';
} 
else if($shipment['rates'][$i]['service'] == 'FEDEX_2_DAY_AM')
{
$bubble_title = 'provides delivery of your planned shipment in just 2 business days by 10:30 a.m. to most U.S. addresses (noon in some ZIP code areas';
$bubble_url = 'http://www.fedex.com/us/fedexupdates/April-2011/2day-am.html';
$rateservicefinaltext ='2 Day AM';
}
else if($shipment['rates'][$i]['service'] == 'FEDEX_2_DAY')
{
$bubble_title = 'By 4:30 p.m. in 2 business days to most areas (by 8 p.m. to residences)
';
$bubble_url = 'http://www.fedex.com/us/fedex/shippingservices/package/2daysaver.html';
}
else if($shipment['rates'][$i]['service'] == 'FEDEX_EXPRESS_SAVER')
{
$bubble_title = 'Delivery by 4:30 p.m. in 3 business days to most areas, by 8 p.m. to residences. Available throughout all states except Alaska and Hawaii
';
$bubble_url = 'http://www.fedex.com/us/fedex/shippingservices/package/2daysaver.html
';
}
else if($shipment['rates'][$i]['service'] == 'Ground')
{
$bubble_title = 'Day-definite delivery typically in one to five days';
$bubble_url = 'http://www.ups.com/content/us/en/shipping/time/service/ground.html ';
}
else if($shipment['rates'][$i]['service'] == 'FEDEX_GROUND')
{
$bubble_title = 'Get cost-effective, day-definite service with transit times supported by a money-back guarantee.*
';
$bubble_url = 'http://www.fedex.com/us/fedex/shippingservices/package/ground.html';

}
else if($shipment['rates'][$i]['service'] == '3DaySelect')
{
$bubble_title = 'Delivery by the end of the third business day';
$bubble_url = 'http://www.ups.com/content/us/en/shipping/time/service/three_day.html?WT.mc_id=VAN701413';
$rateservicefinaltext ='3 Day Select';
}
else if($shipment['rates'][$i]['service'] == '2ndDayAir')
{
$bubble_title = 'Delivery by the end of the second business day - Some locations in Alaska and Hawaii require additional transit time.';
$bubble_url = 'http://www.ups.com/content/us/en/shipping/time/service/second_day.html';
}

else if($shipment['rates'][$i]['service'] == 'NextDayAirSaver')
{
$bubble_title = 'Next business day delivery by end of day';
$bubble_url ='http://www.ups.com/content/us/en/shipping/time/service/next_day_saver.html';
}
else if($shipment['rates'][$i]['service'] == 'NextDayAirEarlyAM')
{
$bubble_title = 'Next business day delivery by 8:00 a.m. to major cities in the 48 contiguous states
Delivery by 8:30, 9:00, 9:30, or 10:00 a.m. to most other U.S. cities, including Anchorage, Alaska
Delivery by 10:30, 11:00, or 11:30 a.m. to additional locations';
$bubble_url = 'http://www.ups.com/content/us/en/shipping/time/service/next_day_am.html';
$rateservicefinaltext ='Next Day Air Early AM';
}
else if($shipment['rates'][$i]['service'] == 'NextDayAir')
{
$bubble_title = 'Next business day delivery by 10:30 a.m., 12:00 noon, or end of day, depending on destination ';
$bubble_url = 'http://www.ups.com/content/us/en/shipping/time/service/next_day.html';
}	
else if($shipment['rates'][$i]['service'] == 'Priority')
{
$bubble_title = 'Get more for your money with fast domestic service in 1, 2, or 3 business days1 based on where your package starts and where its being sen';
$bubble_url = 'https://www.usps.com/ship/priority-mail.htm';
$rateservicefinaltext ='Priority Mail';
}

else if($shipment['rates'][$i]['service'] == 'ParcelSelect')
{
$bubble_title = 'This is the slowest USPS shipping service - You are more concerned with keeping shipping costs low than quick delivery, or when package dimensions exceed limits of other services.';
$bubble_url = 'http://www.stamps.com/usps/parcel-select/';
}
else if($shipment['rates'][$i]['service'] == 'Express')

{
$bubble_title = 'Our fastest domestic service, with limited exceptions, available 365 days a year, with a money-back guarantee1 and overnight scheduled delivery to most U.S. addresses';
$bubble_url = 'https://www.usps.com/ship/first-class-mail.htm';
$rateservicefinaltext ='Priority Mail Express';
}
else if($shipment['rates'][$i]['service'] == 'First')
{
$bubble_title = 'First-Class Mail® is a fast, affordable way to send envelopes and lightweight packages. Its perfect for business and personal correspondence, cards, brochures, and lightweight ';
$bubble_url = 'https://www.usps.com/ship/first-class-mail.htm';
$rateservicefinaltext ='First Class Package Service';
}
if($_SESSION['carbon_neutral']!=''){
$calculaterates = $shipment['rates'][$i]['rate']*1.5/100;
$shipmentrates =  sprintf("%.2f", $shipment['rates'][$i]['rate'] + $calculaterates);
}
else
{
$shipmentrates = $shipment['rates'][$i]['rate'];
}
if($delivery_date!='1'){
if($shipment['rates'][$i]['carrier']=='DHLGlobalMail'){
$checkmark = 'No';
}else{
$checkmark = 'Yes';
}
if($shipment['rates'][$i]['carrier']=='FedEx'){
$carrierlink = "<a href='http://www.fedex.com' target='_blank'>".$shipment['rates'][$i]['carrier']."</a>";
$finalcustomerrate = $shipment['rates'][$i]['list_rate'];
}
else if($shipment['rates'][$i]['carrier']=='USPS'){
$carrierlink = "<a href='http://www.usps.com'  target='_blank'>".$shipment['rates'][$i]['carrier']."</a>";
if($shipment['rates'][$i]['service'] == 'ParcelSelect'){
$finalcustomerrate = number_format($shipment['rates'][$i]['list_rate'] + $shipment['rates'][$i]['list_rate']*20/100,2);

}
else{

$finalcustomerrate = $shipment['rates'][$i]['list_rate'];
}
}
else if($shipment['rates'][$i]['carrier']=='UPS'){
$carrierlink = "<a href='http://www.ups.com'  target='_blank'>".$shipment['rates'][$i]['carrier']."</a>";
$finalcustomerrate = $shipment['rates'][$i]['retail_rate'];
}
else{
$carrierlink = $shipment['rates'][$i]['carrier'];
}
echo "<tr>";
echo "<td class='center' style='text-align:center !important;    padding-top: 0.7% !important;'>".$carrierlink."</td>";
if($shipmentrates!='' && $_SESSION['carbon_neutral']!=''){
echo "<td class='center'  style='text-align:center !important;    padding-top: 0.7% !important;'>".$shipmentrates."</td>";
}else
{
echo "<td class='center'  style='text-align:center !important;    padding-top: 0.7% !important;'>".$finalcustomerrate."</td>";
}
echo "<td class='center'  style='text-align:center !important;    padding-top: 0.7% !important;'><a href='".$bubble_url."' title='".$bubble_title."' data-toggle='tooltip' style='text-transform: capitalize;text-align:left !important;' target='_blank'>".$rateservicefinaltext."</a></td>";
echo "<td class='center' style='text-align:center !important;    padding-top: 0.7% !important;' >".$delivery_date."</td>";
echo "<td class='center' style='text-align:center !important;    padding-top: 0.7% !important;' >".$checkmark."</td>";
echo "<td class='center'  style='text-align:center !important;  padding-top: 0.7% !important;'>
<form name='pricefreightform' id='pricefreightform' action='compare_price_quick.php' method='post'>
<input type='hidden' name='shippingcostbar' value='1'/>
<input type='hidden' name='carrier' value='$carrier' />
<input type='hidden' name='rate' value='$rate' />
<input type='hidden' name='service' value='$rateservicefinaltext' />
<input type='hidden' name='currency' value='$currency' />
<input type='hidden' name='delivery_date' value='$delivery_date' />
<input type='hidden' name='delivery_date_white' value='$delivery_date_white' />
<input type='hidden' name='shipment_id' value='$shipment_id' />
<input type='hidden' name='delivery_date_guaranteed' value='$delivery_date_guaranteed' />
<input type='hidden' name='service_origninal' value='$service' />

<input type='checkbox' name='select_freight'  onClick='this.form.submit();'/>
</form>
</td>
";
echo "</tr> ";
}
}

?>
<?php

}
else
{
echo "<tr><td>No Result Found From Your Search Crietria Please refine your Search</td></tr>";
}
echo '   </tbody>
</table>';





?>

<!--/fluid-row-->

<?php }  catch (Exception $e) {
?>
<?php
if($e->getMessage() !='The requested resource could not be found.'){				
echo $e->getMessage();
}
?>
<?php


}
?>
</div>
</div>

<!--end .accordion-section-content--> 
</div>
<!--end .accordion-section-->
<div class="accordinbarshadow accordion-section-title my_btn_bg accordinbarshadowbottomsubmit">
<div class="bartextalign" style="border-radius:80px 0 0 0;">
<input type="submit" name="calc_rate" class="" value="Submit" onClick="pickuprequestformsubmit()">
</div>
</div>

</div>
</div>
</div>
</div>
<div style="clear:both"></div>
<?php
include('footer.php'); ?>
