<!--
Project Name : Ouiship
Developer : Prashant Gorvadia
 Footer include footer of site and using javascript that not working in header 
-->
<footer class="footer_section">
	<div class="container">
		<div class="row">
			<div class="col-sm-6 col-md-6 col-lg-6">
				<div class="footer_nav">
					<p>COPYRIGHT © 2015 - ALL RIGHTS RESERVED</p>
					<ul>
						<li><a href="#">Home</a></li>
						<li><a href="<?php echo SITE_URL; ?>aboutus.php">About Us</a></li>
						<li><a href="#">Services</a></li>
						<li><a href="<?php echo SITE_URL; ?>terms.php">Terms & Condition</a></li>
                        <li><a href="<?php echo SITE_URL; ?>contactus.php">Contact Us</a></li>
					</ul>
				</div>
			</div>
            <div class="col-sm-6 col-md-6 col-lg-6">
				<div class="social_box">
					<ul>
                        <li><a href="https://www.facebook.com/ouiship"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="https://twitter.com/ouiship"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="https://www.pinterest.com/ouiship/"><i class="fa fa-pinterest"></i></a></li>
                        <li><a href="https://www.linkedin.com/in/ouiship"><i class="fa fa-linkedin"></i></a></li>
					</ul>
				</div>
			</div>
			
		</div>
	</div>
</footer>
<script src="<?php echo JS_PATH; ?>jquery.bxslider.min.js"></script>
</body>
</html>
