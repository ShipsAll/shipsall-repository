<?php
//Project Name : Ouiship
//Developer : Prashant Gorvadia
//page work after payment done easypost purchase happend without selecting pickup service
include('config.php');
include('header.php');
?>
<script type="text/javascript">     
function PrintDiv() {    
var divToPrint = document.getElementById('divToPrint');
var popupWin = window.open('', '_blank', '');
popupWin.document.open();
popupWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</html>');
popupWin.document.close();
}
</script>
<style type="text/css" media="print" >
.nonPrintable {
display:none;
} /*class for the element we don't want to print*/

@page {
size: A4;
margin: 0;
}


</style>
<body>
<div class="nonPrintable">
<?php include('header-menu.php'); ?>
<div class="ch-container"  style="margin-left:30%;">
<div class="box col-md-7" style='background-color: white;'>
<div class="main">
<div class="row">
<?php  
if($_SESSION['x_response_code_new']='1'){ 
if($_SESSION['pickup_request']==''){
require_once("lib/easypost.php");
$fetchapikey = getapikey(); 
\EasyPost\EasyPost::setApiKey($fetchapikey);
$to_address_params = array("name"    => $_SESSION['tofname']." ".$_SESSION['tolname'],
 "street1" => $_SESSION['tostreet1'],
 "street2" => $_SESSION['tostreet2'],
 "city"    => $_SESSION['tocity'],
 "state"   => $_SESSION['tostate'],
 "zip"     => $_SESSION['tozip'],
 "phone"     => $_SESSION['tophone']);
$to_address = \EasyPost\Address::create($to_address_params);

$from_address_params = array("company"    => $_SESSION['fname']." ".$_SESSION['lname'],
   "street1" => $_SESSION['street1'],
   "street2" => $_SESSION['street2'],
   "city"    => $_SESSION['city'],
   "state"   => $_SESSION['state'],
   "zip"     => $_SESSION['zip'],
   "phone"  => $_SESSION['phone']);

$from_address = \EasyPost\Address::create($from_address_params);
// create parcel
$sessweightoz =	$_SESSION['weight_oz']/16;
$sessionweight = $_SESSION['weight']*16;
if($sessionweight == 0 && $_SESSION['weight_oz'] <= 15){
	$sessweight = 8;
}
else if($sessionweight == 0 && $_SESSION['weight'] == 16){
	$sessweight = 16;
}
else

{
$sessweight = $sessionweight + $sessweightoz;
}
$parcel_params = array("length"             => $_SESSION['length'],
"width"              => $_SESSION['width'],
"height"             => $_SESSION['height'],
"weight"             => $sessweight			
);
$parcel = \EasyPost\Parcel::create($parcel_params);	

if($_SESSION['saturdaydelivery']!=''){
$shipment_params = array("from_address" => $from_address,
		   "to_address"   => $to_address,
		   "parcel"       => $parcel,
		   "options"		=> array('saturday_delivery'=>1),
		   );
$shipment = \EasyPost\Shipment::create($shipment_params);

}
else if($_SESSION['insurance']!=''){
$shipment_params = array("from_address" => $from_address,
		   "to_address"   => $to_address,
		   "parcel"       => $parcel);
$shipment = \EasyPost\Shipment::create($shipment_params);

$shipment->insure(array('amount' => $_SESSION['insurance_final_amount'])); 


}
else if($_SESSION['carbon_neutral']!=''){
$shipment_params = array("from_address" => $from_address,
		   "to_address"   => $to_address,
		   "parcel"       => $parcel,
		   "options"		=> array('carbon_neutral'=>1)
		   );
$shipment = \EasyPost\Shipment::create($shipment_params);

}

else if($_SESSION['drondelivery']!=''){
$shipment_params = array("from_address" => $from_address,
		   "to_address"   => $to_address,
		   "parcel"       => $parcel,
		   "options"		=> array('by_drone'=>1),
		   );
$shipment = \EasyPost\Shipment::create($shipment_params);

}
else 
{

$shipment_params = array("from_address" => $from_address,
		   "to_address"   => $to_address,
		   "parcel"       => $parcel
		   );
			   $shipment = \EasyPost\Shipment::create($shipment_params);
			   


}
//

//echo "<pre>";
//print_r($shipment);
//echo "</pre>";
//exit;
//shipmentbuy : start
$shipment_response = $shipment->buy($shipment->lowest_rate($_SESSION['carrier'], $_SESSION['service_origninal']));

$_SESSION['tracking_code'] = $shipment_response->tracking_code;
$_SESSION['shipment_id'] = $shipment->id;
$_SESSION['postage_label'] = $shipment['postage_label']['label_url'];
}
?>
<div class="alert alert-success">
<button type="button" class="close" data-dismiss="alert">&times;</button>
<strong>Thank you for your order!<br>
Check your e-mail or text messages for your Shipping Label. </strong>

</div>

<?php


$nonproforg = selectnonprofitorgname($_SESSION['userid']);
if($_SESSION['carrier']!='' && $_SESSION['service']!='' && $_SESSION['rate']!='' && $_SESSION['tracking_code']!='')
//$orderquery = mysql_query("INSERT INTO TPurchaseOrder(UserId,Carrier,Service,Rate,Label_Url,Tracking_Code,Pickup_Code,Pickup_Id,TOrg_Name) VALUES ('".$_SESSION['userid']."','".$_SESSION['carrier']."','".$_SESSION['service']."','".$_SESSION['rate']."','".$_SESSION['postage_label']."','".$_SESSION['tracking_code']."','".$_SESSION['pickup_confirmation']."','".$_SESSION['Pickup_Id']."','".$nonproforg."')");

$orderquery = mysql_query("INSERT INTO TPackage(TRegistrationId,Length,Width,Height,Weight,WeightOZ,RetailPrice	,LabelURL,TService,TCarrierTypeId,PickupCode,TPickup_Id,TrackingCode,TOrg_Name) VALUES ('".$_SESSION['userid']."','".$_SESSION['length']."','".$_SESSION['width']."','".$_SESSION['height']."','".$_SESSION['weight']."','".$_SESSION['weight_oz']."','".$_SESSION['rate']."','".$_SESSION['postage_label']."','".$_SESSION['service']."','1','".$_SESSION['pickup_confirmation']."','".$_SESSION['Pickup_Id']."','".$_SESSION['tracking_code']."','".$nonproforg."')");
$orderid = mysql_insert_id();


echo "Please refer to the below Tracking Number if you have questions about your order<Br>";
echo "<b>Order Id: ".$orderid."</b><br>";

echo "<b>Tracking Number: : ".$_SESSION['tracking_code']."</b><br>";
if($_SESSION['pickup_confirmation']!=''){
echo "<b>Pickup Confirmation: : ".$_SESSION['pickup_confirmation']."</b><br>";
}
?>
</div>
<br>
<br>
<?php
echo "<div id='divToPrint'><img src=".$_SESSION['postage_label']." height='95%'></div>";
echo " <input type='submit' value='Print This Page' onclick='PrintDiv();'>";


//$tosec = $_SESSION['delivery_confirmation_email'];
//$pickup_res = $pickup->buy(array('carrier'=>$_SESSION['pickup_carrier'], 'service' => $_SESSION['pickup_service']));
$subject = "Congrats Recevied Your Payment Here Is Your Shipment Detail" ;
$message = "";
$message .= "
<html>
<head>
<title>Ouiship Shipment</title>
</head>
<body>
<p><b><font color=black>Here Is Your Order Detail :</font></b><br>
</p>
<br>
<table border='1'>
<tr>
<td><b>Your Tracking Code</b></td>
<td><b>".$_SESSION['tracking_code']."</b></td>
</tr>
</table>";
if($_SESSION['pickup_confirmation']!=''){
$message .= "
<table border='1'>
<tr>
<td><b>Pickup Confirmation</b></td>
<td><b>".$_SESSION['pickup_confirmation']."</b></td>
</tr>
</table>";
}
$message .= "
<Br><Br>
<table>
<tr>
<td>If you need to drop off your package, use the links below to find the closest Package Drop Off</td>
</tr>
<tr>
<td><a href='https://www.fedex.com/locate/'>Federal Express</a></td>
</tr>
<tr>
<td><a href='https://www.ups.com/dropoff'>UPS</a></td>
</tr>
<tr>
<td><a href='https://tools.usps.com/go/POLocatorAction!input.action?&'>US Postal Service </a></td>
</tr>
</tr>
<Br><Br>
<tr>
<td>If you have any questions about your account, please e-mail us at: service@ouiship.com</td>
</tr>
<tr>
<td><a href='http://ouiship.com'><img src=http://www.ouiship.com/images/logo.png></a></td>
</tr>

<tr>
<p>Thank You from Oui! Ship</p>
</tr>					
</table>
</body>
</html>
";

// Always set content-type when sending HTML email
$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

// More headers
$headers .= 'From: <phgorvadia@gmail.com>' . "\r\n";
$headers .= 'Cc: phgorvadia@gmail.com' . "\r\n";
$recipient = $_SESSION['toemail'];
//mail($to,$subject,$message,$headers);

mail($recipient,$subject,$message,$headers);

//email send to client
// }
//  }
}
?>
<div class="nonPrintable"> </div>
</div>
</div>
</div>
<?php

include('footer.php');

$_SESSION['fname'] = '';
$_SESSION['lname'] = '';
$_SESSION['street1'] = '';
$_SESSION['street2'] = '';
$_SESSION['email'] = '';
$_SESSION['city'] = '';
$_SESSION['state'] = '';
$_SESSION['zip'] = '';
$_SESSION['combinecsz'] = '';
$_SESSION['phone'] = '';
$_SESSION['tofname'] = '';
$_SESSION['tolname'] = '';
$_SESSION['tostreet1'] = '';
$_SESSION['tostreet2'] = '';
$_SESSION['tocity'] = '';
$_SESSION['tostate'] = '';
$_SESSION['tozip'] = '';
$_SESSION['tocombinecsz'] = '';
$_SESSION['toemail'] = '';
$_SESSION['tophone'] = '';
$_SESSION['length'] = '';
$_SESSION['width'] = '';
$_SESSION['height'] = '';
$_SESSION['weight'] = '';
$_SESSION['weight_oz'] = '';
$_SESSION['special_handling'] = '';
$_SESSION['special_handling_msg'] = '';
$_SESSION['delivery_confirmation'] = '';
$_SESSION['saturdaydelivery'] = '';
$_SESSION['insurance'] = '';
$_SESSION['insurance_amount'] = '';
$_SESSION['shipment_id'] = '';
$_SESSION['carrier'] = '';
$_SESSION['rate'] = '';
$_SESSION['service'] = '';
$_SESSION['delivery_date'] = '';
$_SESSION['delivery_date_white'] = '';
$_SESSION['delivery_date_guaranteed'] = '';
$_SESSION['service_origninal'] = '';
$_SESSION['display_pickup_start_date'] = '';
$_SESSION['display_pickup_end_date'] = '';
$_SESSION['authorize_pay_amount'] = '';
$_SESSION['tracking_code'] = '';
$_SESSION['postage_label'] = '';
$_SESSION['authorize_transaction_id'] = '';
$_SESSION['authorize_transaction_approved'] = '';
$_SESSION['x_response_code_new']='';

?>
</div>
