<?php 
//Project Name : Ouiship
//Developer : Prashant Gorvadia
//This page include Home page code like slider and other home page related code 
include('config.php');
include('header.php'); 
?>
<body>
<!-- topbar starts -->
<?php include('header-menu.php'); ?>
<!-- topbar ends -->
<div class="row">
    <div class="box col-md-12" >
    <?php  if(isset($_SESSION['philan_instr']) && $_SESSION['philan_instr']=='philan_instr') {?>
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            If you would change the charity/non-profit that we will donate to for your transaction, please go to the Philanthropy Menu Bar and make a new selection. <br>Presently we are donating 10% of the profit from your shipping transactions to Human Rights Watch<br><a href="http://www.ouiship.com/loginsetupacct.php">Click here </a>if you wish to change this.</div>
        </div> 
	<?php $_SESSION['philan_instr']=''; } ?>
</div>
<div class="banner">
    <div class="container">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                <img src="images/slider-final0.jpg" />
                </div>
                <div class="item">
                <img src="images/slider-final1.jpg" />
                </div>
                <div class="item">
                <img src="images/slider-final3.jpg" />
                </div>
            </div>
                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
                </a>
        </div>
    </div>
</div>
<div class="container">
<section class="detail_section">
    <ul>
        <li>
            <a href="<?php echo SITE_URL; ?>ship_package.php?red=ship_package" class="img001"></a>
            </li>
            <li>
            <a href="<?php echo SITE_URL; ?>compare_price.php" class="img002">
            <!--<img src="images/compare-price.png" alt="Ship Package">-->
            </a>
            </li>
            <li style="width:22% !important;">
            <a href="tracking_package.php" class="img003">
            <!--<img src="images/track-package.png" alt="Ship Package">-->
            </a>
        </li>
    </ul>
</section>
</div>
<section class="about_section">
<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12">
            <div class="about_box">
            <div class="about_heading">
            <h3>about us</h3>
            </div>
                <p>
                    What we do is very simple. OuiShip is all about saving you money and time when shipping or returning a package.</p>
                    
                    <p>Don't Throw Your Money Away. We give you the power to choose between the major package shippers—FedEx, UPS, USPS and DHL—to get a discounted price to ship or return a package. When you click on the Ship or Return button, just enter your information--you'll get real-time price quotes to give you the best deals.</p>
                    <p>Your Time is Important. We've also created a platform to help save you time.  When you select the best shipping or return option for your needs, you can choose to have your package picked up. No more standing in long lines.</p>
                    
                    <p>Community. OuiShip combines value and convenience with 'community'. When you ship using our platform, we will donate 10% of our profit to the charity of your selection. If you'd rather send that donation to our carbon-neutral partner, we can do that too.</p>
                    <p>
                    It's Simple. Our service is something that you've been asking for. Discounted shipping and returns based on competition among the carriers, convenience and community.</p>
                </p>
            </div>
        </div>
    </div>
</div>
</section>
<?php include('footer.php'); ?>
