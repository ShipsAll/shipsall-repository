<?php
session_start();
/*echo "<pre>";
print_r($_SESSION['shippment_rate']);
echo "</pre>";
exit;*/
//include('config.php');
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);


if (PHP_SAPI == 'cli')
	die('This example should only be run from a Web Browser');

/** Include PHPExcel */
require_once 'PHPExcel.php';


// Create new PHPExcel object
$objPHPExcel = new PHPExcel();
// Set document properties
$objPHPExcel->getProperties()->setCreator("Yuvraj Jain")
							 ->setLastModifiedBy("Yuvraj Jain")
							 ->setTitle("OUISHIP Confidential Document")
							 ->setSubject("OUISHIP Confidential Document")
							 ->setDescription("OUISHIP Confidential Document")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("OuiShip");


// Add some data
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Zip Code Pair')
            ->setCellValue('B1', 'Weight (lbs)!')
            ->setCellValue('C1', 'Dimensions (inches, LWH)')
            ->setCellValue('D1', 'Originating Zip!')
			->setCellValue('E1', 'Destinating Zip')
			->setCellValue('F1', 'Retail Price')
			->setCellValue('G1', 'USPS Label Cost (to OuiShip)')
			->setCellValue('H1', 'OuiShip Sales Price (to consumer)');
for($i=0;$i<count($_SESSION['shippment_rate']);$i++){	
// Miscellaneous glyphs, UTF-8
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A2', 'Zone 1')
            ->setCellValue('B2', $_SESSION['myweight'])
			->setCellValue('C2', $_SESSION['length'].",".$_SESSION['width'].",".$_SESSION['height'])
			->setCellValue('D2', $_SESSION['fromzip'])
			->setCellValue('E2', $_SESSION['tozip'])
			->setCellValue('F2', $val['retail_rate'])
			->setCellValue('G2', $val['rate'])
			->setCellValue('H2', $val['list_rate']);
 }
// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Non Gty Service');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client's web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="OUISHIP.xls"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;