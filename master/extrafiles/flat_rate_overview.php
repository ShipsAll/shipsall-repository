<?php 
//Project Name : Ouiship
//Developer : Shivam(Delhi)
//This page code for flat rate overview display 
include('config.php');
if(isset($_POST['pickup_request']) ){
	$_SESSION['pickup_request'] = isset($_POST['pickup_request'])?$_POST['pickup_request']:'';
	$_SESSION['pickup_requestfname'] = isset($_POST['pickup_requestfname'])?$_POST['pickup_requestfname']:$_SESSION['pickup_requestfname'];
	$_SESSION['pickup_requestlname'] = isset($_POST['pickup_requestlname'])?$_POST['pickup_requestlname']:$_SESSION['pickup_requestlname'];
	$_SESSION['pickup_requeststreet1'] = isset($_POST['pickup_requeststreet1'])?$_POST['pickup_requeststreet1']:$_SESSION['pickup_requeststreet1'];
	$_SESSION['pickup_requeststreet2'] = isset($_POST['pickup_requeststreet2'])?$_POST['pickup_requeststreet2']:$_SESSION['pickup_requeststreet2'];
	$_SESSION['pickup_requestcity'] = isset($_POST['pickup_requestcity'])?$_POST['pickup_requestcity']:$_SESSION['pickup_requestcity'];
	$_SESSION['pickup_requeststate'] = isset($_POST['pickup_requeststate'])?$_POST['pickup_requeststate']:$_SESSION['pickup_requeststate'];
	$_SESSION['pickup_requestzip'] = isset($_POST['pickup_requestzip'])?$_POST['pickup_requestzip']:$_SESSION['pickup_requestzip'];
	$_SESSION['pickup_requestphone'] = isset($_POST['pickup_requestphone'])?$_POST['pickup_requestphone']:$_SESSION['pickup_requestphone'];
	$_SESSION['pickup_date_starttime'] = isset($_POST['pickup_date_starttime'])?$_POST['pickup_date_starttime']:$_SESSION['pickup_date_starttime'];
	$_SESSION['pickup_date_endtime'] = isset($_POST['pickup_date_endtime'])?$_POST['pickup_date_endtime']:$_SESSION['pickup_date_endtime'];
}
if(isset($_POST['delivery_confirmation']) ){
	$_SESSION['delivery_confirmation'] = isset($_POST['delivery_confirmation'])?$_POST['delivery_confirmation']:'';
	$_SESSION['delivery_confirmation_email'] = isset($_POST['delivery_confirmation_email'])?$_POST['delivery_confirmation_email']:'';
	$_SESSION['delivery_confirmation_phone'] = isset($_POST['delivery_confirmation_phone'])?$_POST['delivery_confirmation_phone']:$_SESSION['delivery_confirmation_phone'];
}

if($_SESSION['userid']==''){
	?>
<META http-equiv="refresh" content="0;URL=loginsetupacct.php">
<?php
exit;
}
include('header.php'); 

if($_SESSION['pickup_requestfname']){

 require_once("lib/easypost.php");
////           

            \EasyPost\EasyPost::setApiKey('LDNaaanQTfoWnVXBIDd8pw');
			
			    $to_address_params = array("name"    => $_SESSION['f_tofname'],
                                       "street1" => $_SESSION['f_tostreet1'],
                                       "street2" => $_SESSION['f_tostreet2'],
                                       "city"    => $_SESSION['f_tocity'],
                                       "state"   => $_SESSION['f_tostate'],
                                       "zip"     => $_SESSION['f_tozip']);
            $to_address = \EasyPost\Address::create($to_address_params);
            
            $from_address_params = array("name"    => $_SESSION['f_fname'],
                                         "street1" => $_SESSION['f_street1'],
                                         "street2" => $_SESSION['f_street2'],
                                         "city"    => $_SESSION['f_city'],
                                         "state"   => $_SESSION['f_state'],
                                         "zip"     => $_SESSION['f_zip']);

            $from_address = \EasyPost\Address::create($from_address_params);

            // create parcel
			$sessweightoz =	$_SESSION['f_weight_oz']/16;
		   $sessionweight = $_SESSION['f_weight']*16;
           $sessweight = $sessionweight + $sessweightoz;
            $parcel_params = array("length"             => 20.2,
                       "width"              => 10.9,
                       "height"             => 5,
                       "weight"             => $sessweight,
					   "predefined_package" =>$flat_rate_selection//'FlatRateLegalEnvelope',//$_POST['menu'],		
					   );
            $parcel = \EasyPost\Parcel::create($parcel_params);	

			$shipment = \EasyPost\Shipment::create(
    array(
        "to_address"   => $to_address,
        "from_address" => $from_address,
        "parcel"       => $parcel
    )
);	 
 $shipment_response = $shipment->buy($shipment->lowest_rate(array($_SESSION['flat_rate_carrier']), array($_SESSION['flat_rate_service'])));
 
$_SESSION['tracking_code'] = $shipment_response->tracking_code;
$_SESSION['shipment_id'] = $shipment->id;
//$_SESSION['tracking_code'] = $shipment_response->tracking_code;
 
 $pickup_address_params = array("name"    => $_SESSION['pickup_requestfname'],
                                         "street1" => $_SESSION['pickup_requeststreet1'],
                                         "street2" => $_SESSION['pickup_requeststreet2'],
                                         "city"    => $_SESSION['pickup_requestcity'],
                                         "state"   => $_SESSION['pickup_requeststate'],
                                         "zip"     => $_SESSION['pickup_requestzip'],
                                         "phone"     => "(968) 743-6776");

$pickup_address = \EasyPost\Address::create($pickup_address_params);

$pickup = \EasyPost\Pickup::create(
    array(
        "address" => $pickup_address,
        "shipment"=> $shipment,
        "reference" =>  $shipment->id,
        "max_datetime" => date("Y-m-d H:i:s"),
        "min_datetime" => date("Y-m-d H:i:s", strtotime('+1 day')),
        "is_account_address" => true,
        "instructions" => "Will be next to garage"
    )
);

if(count($pickup['pickup_rates']) > 0){
$_SESSION['pickup_carrier'] =  $pickup['pickup_rates']['0']['carrier'];
   $_SESSION['pickup_rate'] =  $pickup['pickup_rates']['0']['rate'];
   $_SESSION['pickup_service'] =  $pickup['pickup_rates']['0']['service'];
}
}
?>
<body> <!-- topbar starts -->
    <?php include('header-menu.php'); ?>
    <!-- topbar ends -->

<div class="container mycontainer">
	<div class="shipping_section">
		<h3>flat rate preview</h3>
		<div class="form-top-box">
		<div class="col-sm-6 col-md-6 col-lg-6 from-clr">
			<div class="row">
				<h4>from</h4>
				<address>
                <?php echo $_SESSION['f_fname']."  ".$_SESSION['f_lname']."<br />".$_SESSION['f_street1']."<br />".$_SESSION['f_street2']."<br/>".$_SESSION['f_city']." ".$_SESSION['f_state']." ".$_SESSION['f_zip'].""; ?>
				 <span>info@ouiship.com</span>
				</address>
			</div>
		</div>
		<div class="col-sm-6 col-md-6 col-lg-6 to-clr">
			<div class="row">
				<h4>to</h4>
				<address>
                <?php echo $_SESSION['f_tofname']."  ".$_SESSION['f_tolname']."<br />".$_SESSION['f_tostreet1']."<br />".$_SESSION['f_tostreet2']."<br/>".$_SESSION['f_tocity']." ".$_SESSION['f_tostate']." ".$_SESSION['f_tozip']."";  ?>
				<span>info@ouiship.com</span>
				</address>
			</div>
		</div>
		</div>
		<ul>
			<li>
				<div class="col-sm-3 col-md-3 col-lg-3">
					<div class="row">
						<label>Package :</label>
					</div>
				</div>
				<div class="col-sm-9 col-md-9 col-lg-9">
					<div class="row">
                    
						<div class="pkg_box">
							
							<div class="col-sm-2 col-md-2 col-lg-2">
								<span><small>(LBS)</small> <?php echo $_SESSION['f_weight']; ?></span>
							</div>
							<div class="col-sm-2 col-md-2 col-lg-2">
								<span><small>(OZ)</small> <?php echo $_SESSION['f_weight_oz']; ?></span>
							</div>
						</div>
					</div>
				</div>
			</li>
			<li>
				<div class="col-sm-3 col-md-3 col-lg-3">
					<div class="row">
						<label class="ship_lbl">Shipping Service :</label>
					</div>
				</div>
				<div class="col-sm-9 col-md-9 col-lg-9">
					<div class="row">
						<div class="pkg_box">
							<div class="col-sm-8 col-md-8 col-lg-8">
								<span><small>(Service)</small><?php echo $_SESSION['f_service']; ?></span>
                                <span><small>(Carrier)</small><?php echo $_SESSION['f_carrier']; ?></span>
							</div>
							<div class="col-sm-4 col-md-4 col-lg-4">
								<div class="price">
									<label>Price :</label>
								</div>
					    <span class="arriv">$<?php echo isset($_SESSION['f_rate'])?$_SESSION['f_rate']:0.00; ?></span>
							</div>
						</div>
					</div>
				</div>
			</li>
			<li>
				<div class="col-sm-3 col-md-3 col-lg-3">
					<div class="row">
						<label>Shipping Insurance :</label>
					</div>
				</div>
                <?php
					$insuranceamt = $_SESSION['insurance_amount'];
					$cal_insu_amt = $_SESSION['insurance_amount']*1.5/100;	
				$finalinsuranceamt = isset($cal_insu_amt)?$cal_insu_amt:'0.00';
				?>
				<div class="col-sm-9 col-md-9 col-lg-9">
					<div class="row">
						<div class="pkg_boxchk">
							<div class="col-sm-8 col-md-8 col-lg-8">
								<span><small>(YES <?php if($_SESSION['insurance']!='') {  ?><div class="previewshipcheck"></div><?php } else { ?><div class="previewshipcheckwhite"></div><?php } ?>
               NO <?php if($_SESSION['insurance']=='') {  ?><div class="previewshipcheck"></div><?php } else { ?><div class="previewshipcheckwhite"></div><?php } ?>)</small>&nbsp;<small>Value</small> <?php echo $_SESSION['insurance_amount']; ?></span>
							</div>
							<div class="col-sm-4 col-md-4 col-lg-4">
								<div class="price">
									<label>Price :</label>
								</div>
								<span class="arriv">$<?php echo $finalinsuranceamt; ?></span>
							</div>
						</div>
					</div>
				</div>
			</li>
			 <li>
				<div class="col-sm-3 col-md-3 col-lg-3">
					<div class="row">
						<label>Carbon Neutral :</label>
					</div>
				</div>
				<div class="col-sm-9 col-md-9 col-lg-9">
					<div class="row">
						<div class="pkg_boxchk">
							<div class="col-sm-8 col-md-8 col-lg-8">
                             <span><small>(YES <?php if($_SESSION['carbon_neutral']!='') {  ?><div class="previewshipcheck"></div><?php } else { ?><div class="previewshipcheckwhite"></div><?php } ?>
               NO <?php if($_SESSION['carbon_neutral']=='') {  ?><div class="previewshipcheck"></div><?php } else { ?><div class="previewshipcheckwhite"></div><?php } ?>)</small> </span>
              
								
							</div>
                           
							
                            <div class="col-sm-4 col-md-4 col-lg-4">
								<div class="price">
									<label>Price :</label>
								</div>
								<span class="arriv">$0.00</span>
							</div>
						</div>
					</div>
				</div>
			</li>
			 <li>
				<div class="col-sm-3 col-md-3 col-lg-3">
					<div class="row">
						<label>Pickup Service :</label>
					</div>
				</div>
                <div class="col-sm-9 col-md-9 col-lg-9">
					<div class="row">
                <div class="pkg_boxchk">
							<div class="col-sm-8 col-md-8 col-lg-8">
                             <span><small>(YES <?php if($_SESSION['pickup_rate']!='') {  ?><div class="previewshipcheck"></div><?php } else { ?><div class="previewshipcheckwhite"></div><?php } ?>
               NO <?php if($_SESSION['pickup_rate']=='') {  ?><div class="previewshipcheck"></div><?php } else { ?><div class="previewshipcheckwhite"></div><?php } ?>)</small> </span>
								 <span><?php echo substr($_SESSION['pickup_date_time'],0,10); ?></span>&nbsp;&nbsp;
               <span><small>Between</small>10:00AM</span>&nbsp;&nbsp;
               <span><small>And</small><?php echo substr($_SESSION['pickup_date_time'],-7); ?></span>
							</div>
                             
                            <div class="col-sm-4 col-md-4 col-lg-4">
								<div class="price">
									<label>Price :</label>
								</div>
								<span class="arriv">$<?php echo isset($_SESSION['pickup_rate'])?$_SESSION['pickup_rate']:0.00; ?></span>
							</div>
						</div>
               </div>
				</div>
			</li>
            <li>
				<div class="col-sm-4 col-md-4 col-lg-3 pull-right">
				<div class="row">
				<div class="pkg_box" style="width:109%; float:right;">
					<div class="price">
						<label>Total Price :</label>
					</div>
                    <?php $total_insurance = isset($finalinsuranceamt)?$finalinsuranceamt:'';
$total_pickup_rate = isset($_SESSION['pickup_rate'])?$_SESSION['pickup_rate']:'';



$totalcost = $_SESSION['rate'] + $total_insurance + $total_pickup_rate;
?>
					<span class="arriv">$<?php echo $totalcost; ?></span>
				</div>
				</div>
				</div>
			</li>
			<li>
				<div class="col-sm-3 col-md-3 col-lg-3">
					<div class="row">
						<label>Handling Message :</label>
					</div>
				</div>
				<div class="col-sm-9 col-md-9 col-lg-9">
					<div class="row">
						<div class="pkg_boxchk">
							<div class="col-sm-12 col-md-12 col-lg-12">
                            <span><small>(YES <?php if($_SESSION['special_handling_msg']!='') {  ?><div class="previewshipcheck"></div><?php } else { ?><div class="previewshipcheckwhite"></div><?php } ?>
               NO <?php if($_SESSION['special_handling_msg']=='') {  ?><div class="previewshipcheck"></div><?php } else { ?><div class="previewshipcheckwhite"></div><?php } ?>)</small> <span style="text-transform: uppercase;float:right;"><?php echo $_SESSION['special_handling_msg']; ?></span></span> 
								
							</div>
						</div>
					</div>
				</div>
			</li>
           
            
           
			
            <li>
              
                <div class="col-sm-6 col-md-6 col-lg-6 pickadd" style="padding:0px;">
			<div class="col-sm-6 col-md-6 col-lg-6">
					<div class="row">
						<label>Pickup Address :</label>
					</div>
				</div>
			<div class="col-sm-6 col-md-6 col-lg-6">
					<div class="row">
						<div class="pkg_box">
							<address style="margin-left:10px;min-height: 84px;    max-height: 84px;"><span>
                            <?php echo $_SESSION['pickup_requestfname']."  ".$_SESSION['pickup_requestlname']."<br />".$_SESSION['pickup_requeststreet1']."".$_SESSION['pickup_requeststreet2']." ".$_SESSION['pickup_requestcity']." ".$_SESSION['pickup_requeststate']." <br />".$_SESSION['pickup_requestzip']; ?></span>
				</span></address>
						</div>
					</div>
				</div>
			</div>
                <div class="col-sm-6 col-md-6 col-lg-6 pickadd" style="padding:0px 0px 0px 10px;">
			<div class="col-sm-6 col-md-6 col-lg-6">
					<div class="row">
						<label>Delivery Confirmation :</label>
					</div>
				</div>
			<div class="col-sm-6 col-md-6 col-lg-6">
					<div class="row">
						<div class="pkg_box">
<address style="margin-left:10px; margin-bottom:36px;">
								<span><small>(E-mail)</small> <?php echo $_SESSION['delivery_confirmation_email']; ?></span><Br>
						
								<span><small>(Tel)</small> <?php echo $_SESSION['delivery_confirmation_phone']; ?></span>
							</div>
                            </	address>

						</div>
					</div>
				</div>
			</div>
            </li>
		</ul>
	</div>
</div>     
<div class="bartextalignmain" style="margin-left:23%;">
<div class="bartextalign">
<a href="<?php echo SITE_URL; ?>flat_rate.php"  class="btn btn-info btn-md">Back To Shipping</a>
</div>

<div>
<form action="https://sandbox.paypal.com/cgi-bin/webscr" method="post">
    <input type="hidden" name="business" value="phgorvadia-facilitator@gmail.com">
    <input type="hidden" name="cmd" value="_xclick">
    <input type="hidden" name="item_name" value="<?php echo "Payment For Ouiship Shipment For ".$_SESSION['carrier']; ?>">
    <input type="hidden" name="item_number" value="1">
    <input type="hidden" name="credits" value="510">
    <input type="hidden" name="userid" value="1">
    <input type="hidden" name="amount" value="<?php echo $totalcost; ?>">
    <input type="hidden" name="cpp_header_image" value="http://www.phpgang.com/wp-content/uploads/gang.jpg">
    <input type="hidden" name="no_shipping" value="1">
    <input type="hidden" name="currency_code" value="USD">
    <input type="hidden" name="handling" value="0">
<!-- Identify your business so that you can collect the payments. -->
    <input type="hidden" name="cancel_return" value="<?php echo "http://demo.ouiship.com/DEMO/index.php"; ?>">

    <input type="hidden" name="return" value="<?php echo "http://demo.ouiship.com/DEMO/ship_succ.php"; ?>">
     <input type="hidden" name="notify_url" value="<?php echo "http://demo.ouiship.com/DEMO/ship_succ.php"; ?>">
    <!-- Display the payment button. -->
    <input type="submit" name="submit" border="0" class="btn btn-info btn-md">
<img alt="" border="0" width="1" height="1"
src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" >

</form>
</div>
 
</div>   
<style>
.mycontainer{background-color:#1582c7; padding:20px;    margin-top: 9%;}
.shipping_section{background-color:#03a2e3; float:left; width:100%; padding:20px; box-shadow:0 0 5px 0 #333;}
.shipping_section h3{font-size:44px; font-weight:600; color:#fff; text-shadow:1px 2px 1px #333; text-transform:uppercase; margin:10px 0; padding:10px 0; text-align:center}
.form-top-box{float:left; width:100%; background-color:#13b3f4; border-radius:0 0 10px 10px; box-shadow:0 1px 5px 0 #333;}
.form-top-box .from-clr h4{font-size:28px; text-align:center; line-height:60px; color:#1c74bb; margin:0; box-shadow:0px 3px 3px -2px #333; text-transform:uppercase; font-weight:bold; border-right:2px solid #333; background-color:#26a9e1}
.form-top-box .to-clr h4{font-size:28px; text-align:center; line-height:60px; color:#1c74bb; margin:0; box-shadow:0px 3px 3px -2px #333; text-transform:uppercase; font-weight:bold; background-color:#26a9e1}
.form-top-box .from-clr address{padding:10px; border-right:2px solid #333; margin:0; font-size:16px; font-weight:normal; color:#fff;}
.form-top-box .from-clr address span{float:right;}
.form-top-box .to-clr address{padding:10px; margin:0; font-size:16px; font-weight:normal; color:#fff;}
.form-top-box .to-clr address span{float:right;}
.shipping_section ul{float:left; width:100%; margin:20px 0; padding:0; list-style:none;}
.shipping_section ul li{float:left; width:100%; margin:10px 0;}
.shipping_section ul li label{line-height:35px; color:#fff; font-size:16px; font-weight:normal; background-color:#1a75bb; border-radius:5px; display:block; padding:0 10px; box-shadow:0 1px 2px 0 #333; position:relative; z-index:2; margin-bottom:0;}
.shipping_section ul li label.ship_lbl{margin-top:20px;}
.pkg_box{float:left; width:100%; border-radius:5px; box-shadow: 0 1px 2px 0 #333; position:relative; z-index:0; background-color:#fff; min-height:35px; margin-left:-2px; padding:0;}
.pkg_box span{line-height:35px; font-size:16px; font-weight:600; color:#333;}
.pkg_box span.arriv{color:#9f0e09;}
.pkg_box span small{color:#1a75bb; font-size:16px; margin-right:5%;}
.pkg_box .price{float:left; background-color:#2183c2; padding:0px 0px 0px 20px; margin-right:20px;}
.fifity{height:15px; clear:both}
.shipping_section ul li div.pickadd{line-height:105px;}
.shipping_section ul li div.pickadd label{line-height:105px;}



.bartextalign { width:25%!important;	 } 
.bartextalign3 {width:75%!important; height:72px;} 
.previewshipcheck {
	background: #1a75bb;
padding: 5px;
width: 23px;
height: 14px;
display:inline-block;
    margin-right: 2px;
}
.previewshipcheckwhite {
    background: white;
    padding: 5px;
    width: 23px;
    height: 14px;
    border: 1px solid black;
	display:inline-block;
}

.pkg_boxchk{float:left; width:100%; border-radius:5px; box-shadow: 0 1px 2px 0 #333; position:relative; z-index:0; background-color:#fff; min-height:35px; margin-left:-2px; padding:0;}
.pkg_boxchk span{line-height:35px; font-size:16px; font-weight:600; color:#333;}
.pkg_boxchk span.arriv{color:#9f0e09;}
.pkg_boxchk span small{color:#1a75bb; font-size:16px; }
.pkg_boxchk .price{float:left; background-color:#2183c2; padding:0px 0px 0px 20px; margin-right:20px;}
</style> 
     
  <?php include('footer.php'); ?>