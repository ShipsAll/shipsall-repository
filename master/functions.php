<?php
//Project Name : Ouiship
//Developer : Prashant Gorvadia
//main function file include all function call so with usage of this file we just need to call function and we get all infromation from database
//for fetching first name from registration table : Start
function getfirstname($userid){
	$selectfirstname = "select FirstName,Id from TRegistration where Id=".$userid."";
	$resquery = mysql_query($selectfirstname);
	$firstnamearray = mysql_fetch_array($resquery);
	$firstname = $firstnamearray['FirstName'];
	return $firstname;
}
//for fetching first name from registration table : End
//for fetching zipcoe id from zipcode table : Start
function getzipcodeid($city,$state,$zip){
	$selzipid = "select * from TZipCode where City='".$city."' AND StateCode='".$state."' AND ZIPCode='".$zip."'";
	$selzipidres = mysql_query($selzipid);
	$selzipidresarray = mysql_fetch_array($selzipidres);
	$Id = $selzipidresarray['Id'];
	return $Id;
}
//for fetching zipcoe id from zipcode table : End
//for fetching city/state/zip  from zipcode table : Start
function getzipcodedata($Id){
	$selzipdata = "select * from TZipCode where Id='".$Id."'";
	$selzipdatares = mysql_query($selzipdata);
	$selzipdataresarray = mysql_fetch_array($selzipdatares);
	$cszdata = $selzipdataresarray['City']."~".$selzipdataresarray['StateCode']."~".$selzipdataresarray['ZIPCode'];
	return $cszdata;
}
//for fetching city/state/zip  from zipcode table : End
//for fetching city from zipcode table : Start
function getzipcodecity($Id){
	$selzipdata = "select * from TZipCode where Id='".$Id."'";
	$selzipdatares = mysql_query($selzipdata);
	$selzipdataresarray = mysql_fetch_array($selzipdatares);
	$cszdata = $selzipdataresarray['City'];
	return $cszdata;
}
//for fetching city from zipcode table : End
//for fetching StateCode from zipcode table : Start
function getzipcodestate($Id){
	$selzipdata = "select * from TZipCode where Id='".$Id."'";
	$selzipdatares = mysql_query($selzipdata);
	$selzipdataresarray = mysql_fetch_array($selzipdatares);
	$cszdata = $selzipdataresarray['StateCode'];
	return $cszdata;
}
//for fetching StateCode from zipcode table : End
//for fetching ZIPCode from zipcode table : Start
function getzipcodezip($Id){
	$selzipdata = "select * from TZipCode where Id='".$Id."'";
	$selzipdatares = mysql_query($selzipdata);
	$selzipdataresarray = mysql_fetch_array($selzipdatares);
	$cszdata = $selzipdataresarray['ZIPCode'];
	return $cszdata;
}
//for fetching ZIPCode from zipcode table : End
//for fetching city/state/zip from zipcode table with calling zipcode Id : Start
function getzipcodedatafromzipcode($TZipCodeId){
	$selzipdata = "select * from TZipCode where Id='".$TZipCodeId."'";
	$selzipdatares = mysql_query($selzipdata);
	$selzipdataresarr = mysql_fetch_assoc($selzipdatares);
	$zipcodecombine = $selzipdataresarr['City']."~".$selzipdataresarr['StateCode']."~".$selzipdataresarr['ZIPCode'];
	return $zipcodecombine;
}
//for fetching city/state/zip from zipcode table with calling zipcode Id : End
//for fetching user exist or not from registration table : Start
function chkuserexist($username,$password){
	$seluserexist = "select * from TRegistration where UserName='".$username."' ";
	$seluserexistres = mysql_query($seluserexist);
	$seluserexistresrows = mysql_num_rows($seluserexistres);
	return $seluserexistresrows;
}
//for fetching user exist or not from registration table : End
//for fetching city/state/zip from zipcode table with calling zipcodeId : Start
function wsgetzipcodedata($Id){
	$selzipdata = "select * from TZipCode where ZIPCode='".$Id."'";
	$selzipdatares = mysql_query($selzipdata);
	$selzipdataresarray = mysql_fetch_array($selzipdatares);
	$cszdata = $selzipdataresarray['City']."~".$selzipdataresarray['StateCode']."~".$selzipdataresarray['ZIPCode'];
	return $cszdata;
}
//for fetching city/state/zip from zipcode table with calling zipcodeId : End
//for fetching companyname from philanthropy table : Start
function selectnonprofitorgname($userid){
	$selnonprofitorgname = "select RegistrationId,companyName from TPhilanthropy where RegistrationId=".$userid."";
	$selnonprofitorgnameres = mysql_query($selnonprofitorgname);
	$nonprofitres = mysql_fetch_array($selnonprofitorgnameres);
	return $nonprofitres['companyName'];
}
//for fetching companyname from philanthropy table : End
//for fetching purchased order detail from order table : Start
function selectlatesthistory(){
	$selectlatesthistory = "select * from TPurchaseOrder where UserId=".$_SESSION['userid']." ORDER BY ID DESC LIMIT 1";
	$selectlatesthistoryres = mysql_query($selectlatesthistory);
	$selectlatesthistoryresdetail = mysql_fetch_assoc($selectlatesthistoryres);
	return $selectlatesthistoryresdetail;
}
//for fetching purchased order detail from order table : End
//for fetching all registration detail from registration id : Start
function selectregistrationinfo(){
	$selectregistrationarray = mysql_query("select * from TRegistration where Id=".$_SESSION['userid']."");
	$selectregistrationarrayres = mysql_fetch_assoc($selectregistrationarray);
	return $selectregistrationarrayres;
}
//for fetching all registration detail from registration id : End
//for adding business days to shipment arrival date : Start
function add_business_days($date,$businessdays,$holidays,$dateformat,$increaseday)
{  
	$dayx = strtotime(date('Y-m-d'));  
	$i=1;  
	while($i <= $businessdays){  
	$day = date('N',$dayx);  
	$datestart = date('Y-m-d',$dayx);  
	if($day < 6 && !in_array($datestart,$holidays))$i++;  
	$dayx = strtotime($datestart.$increaseday);  
}  
return date("Y-m-d",strtotime(date('Y-m-d', $dayx) . ' -1 day'));
}
//for adding business days to shipment arrival date : End
//for fetch apikey from Tapikey table : Start
function getapikey(){
	$selnonprofitorgname = "select apikey from Tapikey where Id=1";
	$selnonprofitorgnameres = mysql_query($selnonprofitorgname);
	$nonprofitres = mysql_fetch_array($selnonprofitorgnameres);
	return $nonprofitres['apikey'];
}
//for fetch apikey from Tapikey table : Start
///for quick quote function: start
function selectquickcitystatezip($zipcode){
	$selectquickcitystatezip = "select ZipCode,Name,Address,City,State from TZipCode_Quick where ZipCode=".$zipcode."";
	$selectquickcitystatezipres = mysql_query($selectquickcitystatezip);
	$zipcodearray = mysql_fetch_array($selectquickcitystatezipres);
	return $zipcodearray;
}
///for quick quote function: end
//for creating random number  : start 
function generateRandomString($length = 10) {
	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$charactersLength = strlen($characters);
	$randomString = '';
	for ($i = 0; $i < $length; $i++) {
	$randomString .= $characters[rand(0, $charactersLength - 1)];
}
return $randomString;
}
//for creating random number  : End
//for fetching user detail from Username  : start  
function fetchuserid($email){
	$selectfetchuserid = "select Id,UserName from TRegistration where UserName='".$email."'";
	$selectfetchuseridres = mysql_query($selectfetchuserid);
	$userid = mysql_fetch_array($selectfetchuseridres);
	return $userid['Id'];
}
//for fetching user detail from Username  : End  
//for fetching user name from registration detail id  : start  
function fetchuseremail($RegistrationId){
	$selectfetchuseremail = "select UserName from TRegistration where Id='".$RegistrationId."'";
	$selectfetchuseremailres = mysql_query($selectfetchuseremail);
	$useremail = mysql_fetch_array($selectfetchuseremailres);
	return $useremail['UserName'];
}
//for fetching user name from registration detail id  : End  
//for sending email address : start
function sendemail($email,$subject,$message){
	// Always set content-type when sending HTML email
	$headers = "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
	// More headers
	$headers .= 'From: <phgorvadia@gmail.com>' . "\r\n";
	$headers .= 'Cc: phgorvadia@gmail.com' . "\r\n";
	//mail($to,$subject,$message,$headers);
	mail($tosec,$subject,$message,$headers);
}
//for sending email address : End
//for fetching token detail from Ttoken table : start
function checktokenexist($registrationid){
	$selecttokenexist = "select RegistrationId,Expired from TToken where RegistrationId='".$registrationid."' and Expired='n'";
	$selecttokenexistres = mysql_query($selecttokenexist);
	$tokenresp = mysql_num_rows($selecttokenexistres);
	return $tokenresp;
}
//for fetching token detail from Ttoken table : end
?>