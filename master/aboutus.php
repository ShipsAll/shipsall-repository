<?php
//Project Name : Ouiship
//Developer : Prashant Gorvadia
//About us page include about us page static content 
include('config.php');
include('header.php');
?>
<body>
<?php include('header-menu.php'); ?>

	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-12 col-lg-12">
				<div class="about_box">
					<div class="about_heading">
						<h3>about us</h3>
					</div>
					<p>Oui! Ship is all about you, the consumer shipper!<br><br>

Our goal was to make shipping simple for everyone – not just business users.<br><br>

So we designed a simple yet effective interface, and created some cool shipping tools for PC's and<br> 

mobile devices that allow you to save time and money. No more waiting in line!<br><br>

We feel strongly about returning to the community, and for that reason donate a portion of the profit<br>

from each transaction to a non-profit organization of your choice.<br><br>

Beyond community we are committed to our environment by ensuring the reduction of carbon<br>

emissions. We have established a Carbon Reduction fund that will allow us to support this cause <br>

and make real change.<br><br>

We would love to hear from you if you have any ideas for new features you feel would enhance our<br> 

product. E-mail us at: features@ouiship.com</p>
				</div>
			</div>
			
		</div>
	</div>
<?php
include('footer.php'); ?>