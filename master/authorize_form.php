<?php
//Project Name : Ouiship
//Developer : Yuvraj Jain
//We are using authorize.net for payment, after fullup this page its check to authorize.net and payment done and write now we are using in test mode 
include('config.php');
include('header.php');
//preview page hidden variable store to session : start 
if($_POST['editaddresshidden']=='1'){
	$_SESSION['fname'] = $_REQUEST['fname'];
	$_SESSION['lname'] = $_REQUEST['lname'];		
	$_SESSION['street1'] = $_REQUEST['street1'];
	$_SESSION['street2'] = $_REQUEST['street2'];
	$_SESSION['city'] = $_REQUEST['city'];
	$_SESSION['state'] = $_REQUEST['state'];
	$_SESSION['zip'] = $_REQUEST['zip'];			
}
//preview page hidden variable store to session : end
?>
<body>
<?php include('header-menu.php'); ?>
<div class="container">
    <div class="row">
        <div class="box col-md-12">
            <div class="box-inner">
                <h3>We Accept</h3>
                <img src="<?php echo SITE_URL; ?>images/mastercard.png">
                <img src="<?php echo SITE_URL; ?>images/visa.png">
                <img src="<?php echo SITE_URL; ?>images/american.png">          
                <p>We Accept MasterCard, VISA, American Express credit card as ways to pay.</p>
                <p>We will email you a receipt each time your card is charged.</p>
                <div class="box-header well" data-original-title="" style="padding: 0px 0px 0px 10px;"> 
                <h3>Payment Details</h3>
                </div>
                <div class="box-content">
                    <div class="box col-md-6" style="padding-left: 0px !important;">
                        <div style="border:1px solid black !important;padding: 15px;    background: #f5f5f5;">
                            <div class="form-group">
                                <h4>Billing Contact</h4>
                                </div>
                                <div class="form-group">
                                <label for="exampleInputEmail1">First Name : <?php echo $_SESSION['fname']; ?></label>
                                </div>
                                <div class="form-group">
                                <label for="exampleInputEmail1">Last Name : <?php echo $_SESSION['lname']; ?></label>
                                </div>
                                <div class="form-group">
                                <label for="exampleInputEmail1">Email : <?php echo $_SESSION['username']; ?></label>
                            </div>
                        </div>       
                        <Br>
                        <div style="border:1px solid black !important;padding: 15px;    background: #f5f5f5;">      
                                <div class="form-group">
                                <h4>Billing Address</h4>
                                </div>
                                
                                <div class="form-group">
                                <label for="exampleInputEmail1">Address 1 : <?php echo $_SESSION['street1']; ?></label>
                                </div>
                                <div class="form-group">
                                <label for="exampleInputEmail1">Address 2 : <?php echo $_SESSION['street2']; ?></label>
                                </div>
                                <div class="form-group">
                                <label for="exampleInputEmail1">City : <?php echo $_SESSION['city']; ?></label>
                                </div>
                                
                                <div class="form-group">
                                <label for="exampleInputEmail1">State : <?php echo $_SESSION['state']; ?></label>
                                </div>
                                <div class="form-group">
                                <label for="exampleInputEmail1">Zip Code : <?php echo $_SESSION['zip']; ?></label>
                                </div>
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal" style="padding: 5px;">
                                Edit Address
                                </button>
                                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                  <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Edit Address</h4>
                                </div>
                                <div class="modal-body">
                                  <form name="editaddress" action="<?php SITE_URL; ?>authorize_form.php" method="post"> 
                                <input type="hidden" name="editaddresshidden" value="1">
                                <div class="form-group">
                                <label for="exampleInputEmail1">First Name :  <input type="text" name="fname" value="<?php echo $_SESSION['fname']; ?>" ></label>
                                </div>
                                <div class="form-group">
                                <label for="exampleInputEmail1">Last Name :  <input type="text" name="lname" value="<?php echo $_SESSION['lname']; ?>" ></label>                                </div>
                                <div class="form-group">
                                <label for="exampleInputEmail1">Address 1 :  <input type="text" name="street1" value="<?php echo $_SESSION['street1']; ?>" ></label>
                                </div>
                                <div class="form-group">
                                <label for="exampleInputEmail1">Address 2 :  <input type="text" name="street2" value="<?php echo $_SESSION['street2']; ?>" ></label>
                                </div>                    
                                <div class="form-group">
                                <label for="exampleInputEmail1">City :  <input type="text" name="city" value="<?php echo $_SESSION['city']; ?>" ></label>
                                </div>   
                                <div class="form-group">
                                <label for="exampleInputEmail1">State :  <input type="text" name="state" value="<?php echo $_SESSION['state']; ?>" ></label>
                                </div>
                                <div class="form-group">
                                <label for="exampleInputEmail1">Zip :  <input type="text" name="zip" value="<?php echo $_SESSION['zip']; ?>" ></label>
                                </div>                    
                                 </form>
                                </div>
                                <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="submut" class="btn btn-primary">Save And Continue</button>
                                </div>
                                </div>
                                </div>
                            </div>
                        </div>
                        </div>
                        <div class="box col-md-6"  style="padding-right: 0px !important;">
                        <!-- code for payment information : start -->
                        <!-- code for payment information : end -->
                        
                        <form name="paymentpage" action="<?php SITE_URL; ?>authorize/tests/AuthorizeNetSIM_Test.php" method="post">
                            <input type="hidden" name="authorize_amt" value="<?php echo $_SESSION['paypal_amount']; ?>" />
                            <input type="hidden" name="fname" value="<?php echo $_SESSION['fname']; ?>" />
                            <input type="hidden" name="lname" value="<?php echo $_SESSION['lname']; ?>" />
                            <input type="hidden" name="username" value="<?php echo $_SESSION['username']; ?>" />
                            <input type="hidden" name="street1" value="<?php echo $_SESSION['street1']; ?>" />
                            <input type="hidden" name="street2" value="<?php echo $_SESSION['street2']; ?>" />
                            <input type="hidden" name="city" value="<?php echo $_SESSION['city']; ?>" />
                            <input type="hidden" name="zip" value="<?php echo $_SESSION['zip']; ?>" />
                            <input type="hidden" name="state" value="<?php echo $_SESSION['state']; ?>" />
                            <input type="hidden" name="phone" value="<?php echo $_SESSION['phone']; ?>" />
                            <input type="hidden" name="email" value="<?php echo $_SESSION['email']; ?>" />
                            <?php /*?><div class="form-group">
                            <label for="exampleInputEmail1">Card Number:</label>
                            <input type="text" name="Card_Num" class="form-control" id="exampleInputEmail1" placeholder="Enter 16 Digit Card Number"  maxlength="16"  minlength="16"  required>
                            </div>
                            <div class="form-group">
                            <label for="exampleInputPassword1">Expiration Date:</label>
                            <input type="text"  name="Exp_Date" class="form-control" id="exampleInputPassword1" placeholder="MM/YYYY"  pattern="\d{1,2}/\d{4}" required>
                            </div>
                            <div class="form-group">
                            <label for="exampleInputPassword1">CVV Number:</label>
                            <input type="text"  name="card_code" class="form-control" id="exampleInputPassword1" placeholder="Enter 3 Digit CVV Number"   maxlength="3"  minlength="3" required>
                            </div><?php */?>
                            <input type="submit" name="submit" value="" class="btn btn-default" style="background: url(<?php echo SITE_URL; ?>images/Submit-Payment.png); width: 225px; height: 45px; border: none;">
                        </form>
                        <h3>This Page Is Secure.</h3>
                        <p>Your connection to Oui Ship is always secure as we utilize the latest and strongest security protocols, practices and methodologies to ensure your data is safe. We will never store your credit card data.
                        </p>
                    </div>
            	</div>
        </div>
    </div>
    <!--/span-->
    </div><!--/row-->
</div>
<br><br>
<?php
include('footer.php');
?>

