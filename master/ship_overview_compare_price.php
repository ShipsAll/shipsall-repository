<?php 
//Project Name : Ouiship
//Developer : Prashant Gorvadia
//Page includes easypost purchase when selecting pickup service and showing preview of all selected package services like insurance,carbon neutral,saturday delivery,delivery notification In this page shipment purchase code include because easypost not allowing to show pickup detail before purchase any shipment
include('config.php');
include('header.php'); 
//Pickup bar all information save to session here : start
if($_POST['pickup_fetch_from']=='Yes' && $_POST['pickup_request']=='Yes'){
	$_SESSION['pickup_requestfname_different'] = isset($_POST['pickup_requestfname_different'])?$_POST['pickup_requestfname_different']:'';
	$_SESSION['pickup_requestlname_different'] = isset($_POST['pickup_requestlname_different'])?$_POST['pickup_requestlname_different']:'';
	$_SESSION['pickup_requeststreet1_different'] = isset($_POST['pickup_requeststreet1_different'])?$_POST['pickup_requeststreet1_different']:'';
	$_SESSION['pickup_requeststreet2_different'] = isset($_POST['pickup_requeststreet2'])?$_POST['pickup_requeststreet2']:'';
	$pickup_requestzip_different = explode("~",$_POST['pickup_requestzip_different']);
	$_SESSION['pickup_requestcity_different'] = isset($pickup_requestzip_different['0'])?$pickup_requestzip_different['0']:$_SESSION['pickup_requestcity_different'];
	$_SESSION['pickup_requeststate_different'] = isset($pickup_requeststate_different['1'])?$pickup_requeststate_different['1']:$_SESSION['pickup_requeststate_different'];
	$_SESSION['pickup_requestzip_different'] = isset($pickup_requestzip_different['2'])?$pickup_requestzip_different['2']:$_SESSION['pickup_requestzip_different'];	
	$_SESSION['pickup_requestcombinecsz_different'] = isset($_POST['pickup_requestzip_different'])?$_POST['pickup_requestzip_different']:$_SESSION['pickup_requestcombinecsz_different'];	
	$_SESSION['pickup_requestphone_different'] = isset($_POST['pickup_requestphone_different'])?$_POST['pickup_requestphone_different']:'';
	$_SESSION['pickup_request_msg'] = isset($_POST['pickup_request_msg'])?$_POST['pickup_request_msg']:'';
	$_SESSION['pickup_date_starttime'] = isset($_POST['pickup_date_starttime'])?$_POST['pickup_date_starttime']:'';
	$_SESSION['pickup_date_endtime'] = isset($_POST['pickup_date_endtime'])?$_POST['pickup_date_endtime']:'';
	$_SESSION['pickup_request'] = isset($_POST['pickup_request'])?$_POST['pickup_request']:$_SESSION['pickup_request'];
	$_SESSION['pickup_fetch_from'] = isset($_POST['pickup_fetch_from'])?$_POST['pickup_fetch_from']:$_SESSION['pickup_fetch_from'];
	$_SESSION['pickup_requestfname'] = '';
	$_SESSION['pickup_requestlname'] = '';
	$_SESSION['pickup_requeststreet1'] = '';
	$_SESSION['pickup_requeststreet2'] = '';
	$_SESSION['pickup_requestcity'] = '';
	$_SESSION['pickup_requeststate'] = '';
	$_SESSION['pickup_requestzip'] = '';
	$_SESSION['pickup_requestphone'] = '';
}
else if($_POST['pickup_request']=='Yes')
{
	$_SESSION['pickup_requestfname'] = isset($_SESSION['fname'])?$_SESSION['fname']:'';
	$_SESSION['pickup_requestlname'] = isset($_SESSION['lname'])?$_SESSION['lname']:'';
	$_SESSION['pickup_requeststreet1'] = isset($_SESSION['street1'])?$_SESSION['street1']:'';
	$_SESSION['pickup_requeststreet2'] = isset($_SESSION['street2'])?$_SESSION['street2']:'';
	$_SESSION['pickup_requestcity'] = isset($_SESSION['city'])?$_SESSION['city']:'';
	$_SESSION['pickup_requeststate'] = isset($_SESSION['state'])?$_SESSION['state']:'';
	$_SESSION['pickup_requestzip'] = isset($_SESSION['zip'])?$_SESSION['zip']:'';
	$_SESSION['pickup_requestphone'] = isset($_SESSION['phone'])?$_SESSION['phone']:'';
	$_SESSION['pickup_request_msg'] = isset($_POST['pickup_request_msg'])?$_POST['pickup_request_msg']:'';
	$_SESSION['pickup_date_starttime'] = isset($_POST['pickup_date_starttime'])?$_POST['pickup_date_starttime']:'';
	$_SESSION['pickup_date_endtime'] = isset($_POST['pickup_date_endtime'])?$_POST['pickup_date_endtime']:'';
	$_SESSION['pickup_request'] = isset($_POST['pickup_request'])?$_POST['pickup_request']:$_SESSION['pickup_request'];
	
	$_SESSION['pickup_requestfname_different'] = '';
	$_SESSION['pickup_requestlname_different'] = '';
	$_SESSION['pickup_requeststreet1_different'] = '';
	$_SESSION['pickup_requeststreet2_different'] = '';
	$_SESSION['pickup_requestcity_different'] = '';
	$_SESSION['pickup_requeststate_different'] = '';
	$_SESSION['pickup_requestzip_different'] = '';
	$_SESSION['pickup_requestphone_different'] = '';

}
$pickup_start_date = date("Y-m-d H:i:s", strtotime($_SESSION['pickup_date_starttime']));
$pickup_end_date = date("Y-m-d H:i:s", strtotime($_SESSION['pickup_date_endtime']));
$pickup_start_date_format = date("m/d/Y H:i:s", strtotime($_SESSION['pickup_date_starttime']));
$pickup_end_date_format = date("m/d/Y H:i:s", strtotime($_SESSION['pickup_date_endtime']));
$_SESSION['display_pickup_start_date'] = $_SESSION['pickup_date_starttime'];
$_SESSION['display_pickup_end_date'] = $_SESSION['pickup_date_endttime'];
//Pickup bar all information save to session here : start
//If user not logged in so its redirect to login page : start
if($_SESSION['userid']==''){
	header('Location:loginsetupacct.php');
}
//If user not logged in so its redirect to login page : end
//Shipment Insurance calculation Code : Start
$insuranceamt = $_SESSION['insurance_amount'];
$cal_insu_amt = $_SESSION['insurance_amount']*1.5/100;	
if($cal_insu_amt!=''){
	$finalinsuranceamt=number_format($cal_insu_amt,2);
}
else
{
	$finalinsuranceamt='0.00';
}

$_SESSION['insurance_final_amount'] = $finalinsuranceamt;
//Shipment Insurance calculation Code : End
if($_SESSION['pickup_request']=='Yes'){
//Easypost Api code : Start
require_once("lib/easypost.php");
$fetchapikey = getapikey();  
//Below API function for fetching api code from database : Start
\EasyPost\EasyPost::setApiKey($fetchapikey);
//Below API function for fetching api code from database : End
//here is code of shipment purchase as per easypost documantation : Start
	 $to_address_params = array("name"    => $_SESSION['tofname']." ".$_SESSION['tolname'],
						   "street1" => $_SESSION['tostreet1'],
						   "street2" => $_SESSION['tostreet2'],
						   "city"    => $_SESSION['tocity'],
						   "state"   => $_SESSION['tostate'],
						   "zip"     => $_SESSION['tozip'],
						   "phone"     => $_SESSION['tophone']);
$to_address = \EasyPost\Address::create($to_address_params);

$from_address_params = array("company"    => $_SESSION['fname']." ".$_SESSION['lname'],
							 "street1" => $_SESSION['street1'],
							 "street2" => $_SESSION['street2'],
							 "city"    => $_SESSION['city'],
							 "state"   => $_SESSION['state'],
							 "zip"     => $_SESSION['zip'],
							 "phone"  => $_SESSION['phone']);

$from_address = \EasyPost\Address::create($from_address_params);
//Weight calculation coems here : Start
$sessweightoz =	$_SESSION['weight_oz']/16;
$sessionweight = $_SESSION['weight']*16;
if($sessionweight == 0 && $_SESSION['weight_oz'] <= 15){
	$sessweight = 8;
}
else if($sessionweight == 0 && $_SESSION['weight'] == 16){
	$sessweight = 16;
}
else
{
	$sessweight = $sessionweight + $sessweightoz;
}
//Weight calculation coems here : Start
$parcel_params = array("length"             => $_SESSION['length'],
					   "width"              => $_SESSION['width'],
					   "height"             => $_SESSION['height'],
					   "weight"             => $sessweight			
);
$parcel = \EasyPost\Parcel::create($parcel_params);	
//here is if condition for different shipping optional service provided for shipment : Start
if($_SESSION['saturdaydelivery']!=''){
	$shipment_params = array("from_address" => $from_address,
							 "to_address"   => $to_address,
							 "parcel"       => $parcel,
							 "options"		=> array('saturday_delivery'=>1),
								 );
	$shipment = \EasyPost\Shipment::create($shipment_params);
}
else if($_SESSION['insurance']!=''){
	$shipment_params = array("from_address" => $from_address,
										 "to_address"   => $to_address,
										 "parcel"       => $parcel);
		 
	$shipment = \EasyPost\Shipment::create($shipment_params);
	$shipment->insure(array('amount' => $_SESSION['insurance_final_amount'])); 
}
else if($_SESSION['carbon_neutral']!=''){
	$shipment_params = array("from_address" => $from_address,
							 "to_address"   => $to_address,
							 "parcel"       => $parcel,
							 "options"		=> array('carbon_neutral'=>1)
							 );
	$shipment = \EasyPost\Shipment::create($shipment_params);
}

else if($_SESSION['drondelivery']!=''){
	$shipment_params = array("from_address" => $from_address,
							 "to_address"   => $to_address,
							 "parcel"       => $parcel,
							 "options"		=> array('by_drone'=>1),
							 );
	$shipment = \EasyPost\Shipment::create($shipment_params);
}
else 
{
	$shipment_params = array("from_address" => $from_address,
							 "to_address"   => $to_address,
							 "parcel"       => $parcel
							 );
	$shipment = \EasyPost\Shipment::create($shipment_params);

}
//here is if condition for different shipping optional service provided for shipment : End
//Using below code we are purchase shipment : Start
$shipment_response = $shipment->buy($shipment->lowest_rate($_SESSION['carrier'], $_SESSION['service_origninal']));
//Using below code we are purchase shipment : End
$_SESSION['tracking_code'] = $shipment_response->tracking_code;
$_SESSION['shipment_id'] = $shipment->id;
$_SESSION['postage_label'] = $shipment['postage_label']['label_url'];
}
$_SESSION['tracking_code'] = $shipment_response->tracking_code;
//here is code of shipment purchase as per easypost documantation : End
///here is code for pickup purchased as per easypost documantation : Start
if($_SESSION['pickup_request']=='Yes'){
if($_SESSION['pickup_requestfname_different']!=''){
$pickup_address_params = array("name"    => $_SESSION['pickup_requestfname_different']." ".$_SESSION['pickup_requestlname_different'],
							 "street1" => $_SESSION['pickup_requeststreet1_different'],
							 "street2" => $_SESSION['pickup_requeststreet2_different'],
							 "city"    => $_SESSION['pickup_requestcity_different'],
							 "state"   => $_SESSION['pickup_requeststate_different'],
							 "zip"     => $_SESSION['pickup_requestzip_different'],
							 "phone"     => $_SESSION['pickup_requestphone_different']);
}
else
{
$pickup_address_params = array("name"    => $_SESSION['pickup_requestfname']." ".$_SESSION['pickup_requestlname'],
							 "street1" => $_SESSION['pickup_requeststreet1'],
							 "street2" => $_SESSION['pickup_requeststreet2'],
							 "city"    => $_SESSION['pickup_requestcity'],
							 "state"   => $_SESSION['pickup_requeststate'],
							 "zip"     => $_SESSION['pickup_requestzip'],
							 "phone"     => $_SESSION['pickup_requestphone']);
}
$pickup_address = \EasyPost\Address::create($pickup_address_params);


$pickup = \EasyPost\Pickup::create(
array(
"address" => $pickup_address,
"shipment"=> $shipment,
"reference" =>  $shipment->id,
"max_datetime" => $pickup_end_date,
"min_datetime" => $pickup_start_date,
"is_account_address" => false,
"instructions" => $_SESSION['pickup_request_msg']
)
);

if(count($pickup['pickup_rates']) > 0){
$_SESSION['pickup_carrier'] =  $pickup['pickup_rates']['0']['carrier'];

$_SESSION['Pickup_Id'] =  $pickup['pickup_rates']['0']['id'];
$_SESSION['pickup_rate'] =  $pickup['pickup_rates']['0']['rate'];
$_SESSION['pickup_service'] =  $pickup['pickup_rates']['0']['service'];
}

$pickup_res = $pickup->buy(array('carrier'=>$_SESSION['pickup_carrier'], 'service' =>$_SESSION['pickup_service']));
}
$_SESSION['pickup_confirmation'] = $pickup->confirmation;
///here is code for pickup purchased as per easypost documantation : Start
//Easypost Api code : End
?>
<body>
<!-- topbar starts -->
<?php include('header-menu.php'); ?>
<!-- topbar ends -->

<div class="container mycontainer">
    <div class="shipping_section">
        <h3>shipping preview</h3>
        <div class="form-top-box">
        <!-- here is division for sender : start -->
        <div class="col-sm-6 col-md-6 col-lg-6 from-clr">
        <div class="row">
        <h4>Sender</h4>
        <address>
        <div style="float:left">
        <?php 
            if($_SESSION['street2']!=''){
          echo $_SESSION['fname']."  ".$_SESSION['lname']."<br />".$_SESSION['street1']."<br />".$_SESSION['street2']."<br/>".$_SESSION['city']." ".$_SESSION['state']." ".$_SESSION['zip']."";
            }else{
                 echo $_SESSION['fname']."  ".$_SESSION['lname']."<br />".$_SESSION['street1']."<br />".$_SESSION['city']." ".$_SESSION['state']." ".$_SESSION['zip']."";
            }
           ?>
        </div>
        <div style="float:right;position: relative;top: 18px;"> <span><?php echo $_SESSION['email'] ?></span><br>
        <span><?php echo $_SESSION['phone'] ?></span> </div>
        </address>
        </div>
        </div>
        <!-- here is division for sender : end -->
        <!-- here is division for recipient : start -->
         <div class="col-sm-6 col-md-6 col-lg-6 to-clr recipientbgcolor">
        <div class="row">
        <h4>Recipient</h4>
        <address>
        <div style="float:left;">
        <?php 
            if($_SESSION['tostreet2']!=''){
            echo $_SESSION['tofname']."  ".$_SESSION['tolname']."<br />".$_SESSION['tostreet1']."<br />".$_SESSION['tostreet2']."<br/>".$_SESSION['tocity']." ".$_SESSION['tostate']." ".$_SESSION['tozip']."";
            }else{
            echo $_SESSION['tofname']."  ".$_SESSION['tolname']."<br />".$_SESSION['tostreet1']."<br />".$_SESSION['tocity']." ".$_SESSION['tostate']." ".$_SESSION['tozip']."";
            }?>
        </div>
        <div style="float:right;position: relative;top: 18px;"> <span><?php echo $_SESSION['toemail']; ?></span><br>
        <span><?php echo $_SESSION['tophone'] ?></span> </div>
        </address>
        </div>
        </div>   
         <!-- here is division for recipient : end -->
        </div>
        <ul>
        <li>
         <!-- here is division for package : start -->
        <div class="col-sm-3 col-md-3 col-lg-3">
        <div class="row">
        <label>Package :</label>
        </div>
        </div>
        <div class="col-sm-9 col-md-9 col-lg-9">
        <div class="row">
        <div class="pkg_box">
        <div class="col-sm-2 col-md-2 col-lg-2"> <span><small>(L)</small> <?php echo $_SESSION['length']; ?> inches</span> </div>
        <div class="col-sm-2 col-md-2 col-lg-2"> <span><small>(H)</small> <?php echo $_SESSION['height']; ?> inches</span> </div>
        <div class="col-sm-3 col-md-3 col-lg-3"> <span><small>(W)</small> <?php echo $_SESSION['width']; ?> inches</span> </div>
        <div class="col-sm-2 col-md-2 col-lg-2"> <span><small>(LBS)</small> <?php echo floor($_SESSION['weight']); ?></span> </div>
        <div class="col-sm-2 col-md-2 col-lg-2"> <span><small>(OZ)</small> <?php echo $_SESSION['weight_oz']; ?></span> </div>
        
        </div>
        </div>
        </div>
        <!-- here is division for package : start -->
        </li>
        <li>
        <!-- here is division for shipping service : start -->
        <div class="col-sm-3 col-md-3 col-lg-3">
        <div class="row">
        <label class="ship_lbl">Shipping Service :</label>
        </div>
        </div>
        <div class="col-sm-9 col-md-9 col-lg-9">
        <div class="row">
        <div class="pkg_box">
        <div class="col-sm-4 col-md-4 col-lg-4"> <span><small>(Shipped)</small><?php echo $_SESSION['week_start_date']; ?></span> </div>
        <div class="col-sm-4 col-md-4 col-lg-4"> <span class="arriv"><small>(Est Arrival)</small><?php echo $_SESSION['delivery_date']; ?></span> </div>
        <div class="col-sm-4 col-md-4 col-lg-4"> <span><small>(Carrier)</small><?php echo $_SESSION['carrier']; ?></span> </div>
        </div>
        <div class="fifity"></div>
        <div class="pkg_box">
        <div class="col-sm-8 col-md-8 col-lg-8"> <span style="text-transform:capitalize">
          <div><?php echo $_SESSION['service']; ?></div>
          </span> </div>
        <div class="col-sm-4 col-md-4 col-lg-4">
          <div class="price">
            <label>Price :</label>
          </div>
          <?php 
                            ?>
          <span class="arriv">$<?php echo  $_SESSION['rate']; ?></span> </div>
        </div>
        </div>
        </div>
        <!-- here is division for shipping service : End -->
        </li>
        <!-- here is division for shipping insurance : start -->
        <li>
        <div class="col-sm-3 col-md-3 col-lg-3">
        <div class="row">
        <label>Shipping Insurance :</label>
        </div>
        </div>
        <div class="col-sm-9 col-md-9 col-lg-9">
        <div class="row">
        <div class="pkg_boxchk">
        <div class="col-sm-8 col-md-8 col-lg-8"> <span><small>(YES
          <?php if($_SESSION['insurance']!='') {  ?>
          <div class="previewshipcheck"></div>
          <?php } else { ?>
          <div class="previewshipcheckwhite"></div>
          <?php } ?>
          NO
          <?php if($_SESSION['insurance']=='') {  ?>
          <div class="previewshipcheck"></div>
          <?php } else { ?>
          <div class="previewshipcheckwhite"></div>
          <?php } ?>
          )</small>&nbsp;
          <?php if($_SESSION['insurance_amount']!='') { ?>
          Value: $
          <?php } ?>
          <?php echo isset($_SESSION['insurance_amount'])?$_SESSION['insurance_amount']:'0.00'; ?></span> </div>
        <div class="col-sm-4 col-md-4 col-lg-4">
          <div class="price">
            <label>Price :</label>
          </div>
          <span class="arriv">$<?php echo $finalinsuranceamt; ?></span> </div>
        </div>
        </div>
        </div>
         <!-- here is division for shipping insurance : end -->
        </li>
        <li>
        <!-- here is division for carbon neutral : start -->
        <div class="col-sm-3 col-md-3 col-lg-3">
        <div class="row">
        <label>Carbon Neutral :</label>
        </div>
        </div>
        <div class="col-sm-9 col-md-9 col-lg-9">
        <div class="row">
        <div class="pkg_boxchk">
        <div class="col-sm-8 col-md-8 col-lg-8"> <span><small>(YES
          <?php if($_SESSION['carbon_neutral']!='') {  ?>
          <div class="previewshipcheck"></div>
          <?php } else { ?>
          <div class="previewshipcheckwhite"></div>
          <?php } ?>
          NO
          <?php if($_SESSION['carbon_neutral']=='') {  ?>
          <div class="previewshipcheck"></div>
          <?php } else { ?>
          <div class="previewshipcheckwhite"></div>
          <?php } ?>
          )</small> </span> </div>
        <?php if($_SESSION['carbon_neutral']!='') { 
                       $total_carbon_neutral_main='0.50';
                        } else { 
                        $total_carbon_neutral_main='0.00';
                         } ?>
        <div class="col-sm-4 col-md-4 col-lg-4">
          <div class="price">
            <label>Price :</label>
          </div>
          <span class="arriv">$<?php echo $total_carbon_neutral_main; ?></span> </div>
        </div>
        </div>
        </div>
        <!-- here is division for carbon neutral : end -->
        </li>
        <li>
		<!-- here is division for pickup service : start -->
        <div class="col-sm-3 col-md-3 col-lg-3">
        <div class="row">
        <label>Pickup Service :</label>
        </div>
        </div>
        <!-- here is division for pickup service : end -->
        <div class="col-sm-9 col-md-9 col-lg-9">
        <div class="row">
        <div class="pickuppkg_boxchk">
        <div class="col-sm-8 col-md-8 col-lg-8"> <span><small>(YES
          <?php if($_SESSION['pickup_rate']!='') {  ?>
          <div class="previewshipcheck"></div>
          <?php } else { ?>
          <div class="previewshipcheckwhite"></div>
          <?php } ?>
          NO
          <?php if($_SESSION['pickup_rate']=='') {  ?>
          <div class="previewshipcheck"></div>
          <?php } else { ?>
          <div class="previewshipcheckwhite"></div>
          <?php } ?>
          )</small>&nbsp;&nbsp;
          <?php if($_SESSION['pickup_request_msg']!='') { echo "Pickup Message: "; } ?>
          <span style="color:#ef1e26;text-transform: capitalize;"><?php echo $_SESSION['pickup_request_msg']; ?></span></span><br>
          <span><?php echo $_SESSION['pickup_service']; ?> </span><br>
          <?php  if($_SESSION['pickup_rate']!='') { ?>
          <span><small>Between</small>&nbsp;:<?php echo $_SESSION['pickup_date_starttime']; ?></span> <span><small>And</small>&nbsp;:<?php echo $_SESSION['pickup_date_endtime']; ?></span>
          <?php } ?>
        </div>
        <?php
                         if($_SESSION['pickup_rate']!=''){
                             $pickup_rate = $_SESSION['pickup_rate'];
                         }else{
                             $pickup_rate = '0.00';
                         }
                         ?>
        <div class="col-sm-4 col-md-4 col-lg-4">
          <div class="price">
            <label>Price :</label>
          </div>
          <span class="arriv">$<?php echo $pickup_rate; ?></span> </div>
        </div>
        </div>
        </div>
        </li>
        <li>
        <!-- here is division for total price : start -->
        <div class="col-sm-4 col-md-4 col-lg-3 pull-right">
        <div class="row">
        <div class="pkg_box" style="width:109%; float:right;">
        <div class="price">
          <label>Total Price :</label>
        </div>
        <?php $total_insurance = isset($finalinsuranceamt)?$finalinsuranceamt:'0.00';
        $total_pickup_rate = isset($_SESSION['pickup_rate'])?$_SESSION['pickup_rate']:'0.00';
        
        if($_SESSION['carbon_neutral']!=''){
        $total_carbon_neutral = 0.50;
        }
        
        
        
        $totalcost = $_SESSION['rate'] + $total_insurance + $total_pickup_rate + $total_carbon_neutral;
        $_SESSION['paypal_amount'] =  $_SESSION['rate'] + $total_insurance + $total_pickup_rate + $total_carbon_neutral;
        ?>
        <span class="arriv">$<?php echo $totalcost; ?></span> </div>
        </div>
        </div>
        <!-- here is division for total price : end -->
        </li>
        <li>
        <!-- here is division for handling message : start -->
        <div class="col-sm-3 col-md-3 col-lg-3">
        <div class="row">
        <label>Handling Message :</label>
        </div>
        </div>
        <div class="col-sm-9 col-md-9 col-lg-9">
        <div class="row">
        <div class="pkg_boxchk">
        <div class="col-sm-12 col-md-12 col-lg-12"> <span><small>(YES
          <?php if($_SESSION['special_handling_msg']!='') {  ?>
          <div class="previewshipcheck"></div>
          <?php } else { ?>
          <div class="previewshipcheckwhite"></div>
          <?php } ?>
          NO
          <?php if($_SESSION['special_handling_msg']=='') {  ?>
          <div class="previewshipcheck"></div>
          <?php } else { ?>
          <div class="previewshipcheckwhite"></div>
          <?php } ?>
          )</small> <span style="text-transform: uppercase;float:right;"><?php echo $_SESSION['special_handling_msg']; ?></span></span> </div>
        </div>
        </div>
        </div>
        <!-- here is division for total price : end -->
        </li>
        <li>
        <!-- here is division for pickup address : start -->
        <div class="col-sm-6 col-md-6 col-lg-6 pickadd" style="padding:0px;">
        <div class="col-sm-6 col-md-6 col-lg-6">
        <div class="row">
        <label>Pickup Address :</label>
        </div>
        </div>
        <div class="col-sm-6 col-md-6 col-lg-6">
        <div class="row">
        <div class="pkg_box">
          <address style="margin-left:10px;min-height: 84px;    max-height: 84px;">
          <span>
          <?php
                        if($_SESSION['pickup_request']=='Yes'){
        if($_SESSION['pickup_requestfname_different']!=''){
                        
                            echo $_SESSION['pickup_requestfname_different']."  ".$_SESSION['pickup_requestlname_different']."<br />".$_SESSION['pickup_requeststreet1_different']."<br />".$_SESSION['pickup_requestcity_different']." ".$_SESSION['pickup_requeststate_different']." ".$_SESSION['pickup_requestzip_different']; 
                        
        }else
        {
        echo $_SESSION['pickup_requestfname']."  ".$_SESSION['pickup_requestlname']."<br />".$_SESSION['pickup_requeststreet1']."<br />".$_SESSION['pickup_requestcity']." ".$_SESSION['pickup_requeststate']." ".$_SESSION['pickup_requestzip']; 
        }
                        }
                        ?>
          </span> </span>
          </address>
        </div>
        </div>
        </div>
        </div>
        <!-- here is division for pickup address : end -->
        <!-- here is division for Delivery Confirmation : start -->
        <div class="col-sm-6 col-md-6 col-lg-6 pickadd" style="padding:0px 0px 0px 10px;">
        <div class="col-sm-6 col-md-6 col-lg-6">
        <div class="row">
        <label>Delivery Confirmation :</label>
        </div>
        </div>
        <div class="col-sm-6 col-md-6 col-lg-6">
        <div class="row">
        <div class="delivery_pkg_box">
          <address style="margin-left:10px; margin-bottom:36px;">
          <span><small>(E-mail)</small> <?php echo isset($_SESSION['delivery_confirmation_email'])?$_SESSION['delivery_confirmation_email']:$_SESSION['toemail']; ?> </span><Br>
          <span><small>(Tel)</small> <?php echo isset($_SESSION['delivery_confirmation_phone'])?$_SESSION['delivery_confirmation_phone']:$_SESSION['tophone']; ?></span> </div>
        </address>
        </div>
        </div>
        </div>
         <!-- here is division for Delivery Confirmation : end -->
        </li>
        <li>
         <!-- here is division for Tracking Number : start -->
        <div class="col-sm-3 col-md-3 col-lg-3">
        <div class="row">
        <label>Tracking Number :</label>
        </div>
        </div>
        <div class="col-sm-9 col-md-9 col-lg-9">
        <div class="row">
        <div class="pkg_boxchk">
        <div class="col-sm-12 col-md-12 col-lg-12"> <span><?php echo $_SESSION['tracking_code']; ?></span> </div>
        </div>
        </div>
        </div>
         <!-- here is division for Tracking Number : end -->
        </li>
        <li>
         <!-- here is division for Pickup Confirmation : start -->
        <div class="col-sm-3 col-md-3 col-lg-3">
        <div class="row">
        <label>Pickup Confirmation :</label>
        </div>
        </div>
        <div class="col-sm-9 col-md-9 col-lg-9">
        <div class="row">
        <div class="pkg_boxchk">
        <div class="col-sm-12 col-md-12 col-lg-12"> <span><?php echo $_SESSION['pickup_confirmation']; ?></span> </div>
        </div>
        </div>
        </div>
         <!-- here is division for Pickup Confirmation : end -->
        </li>
        </div>
        </ul>
        </div>
        </div>
         <!-- here is division for Back to Shipping and proceed to payment : start -->
        <div class="ship_overview_bottom_main" >
        <div class="ship_overview_bottom_left"> <a href="<?php echo SITE_URL; ?>compare_price.php" ><img src="<?php echo SITE_URL; ?>images/back-to-shpping.png"></a> </div>
        <div  class="ship_overview_bottom_right">
        <form name="authorize_payment" action="authorize_form.php" method="post">
        <input type="hidden" name="authorize_amount" value="<?php echo $totalcost; ?>">
        
        <!-- Display the payment button. -->
        <input type="submit" name="submit" border="0" style="background: url(<?php echo SITE_URL; ?>images/Proceed-To-Payment.png);
        width: 225px;
        height: 45px;
        border: none;" value="">
        </form>
    </div>
</div>
		<!-- here is division for Back to Shipping and proceed to payment : end -->
<style>
.mycontainer{background-color:black; padding:20px;    margin-top: 9%;}
.shipping_section{background-color:#e1e3e4; float:left; width:100%; padding:20px; box-shadow:0 0 5px 0 #333;}
.shipping_section h3{font-size:44px; font-weight:600; color:#000; text-shadow:1px 2px 1px #333; text-transform:uppercase; margin:10px 0; padding:10px 0; text-align:center}
.form-top-box{float:left; width:100%; background-color:#fff; border-radius:0 0 10px 10px; box-shadow:0 1px 5px 0 #333;}
.form-top-box .from-clr h4{font-size:28px; text-align:center; line-height:60px; color:#FFF; margin:0; box-shadow:0px 3px 3px -2px #333; text-transform:uppercase; font-weight:bold; border-right:2px solid #333; background-color:#26a9e1}
.form-top-box .to-clr h4{font-size:28px; text-align:center; line-height:60px; color:#FFF; margin:0; box-shadow:0px 3px 3px -2px #333; text-transform:uppercase; font-weight:bold; background-color:#26a9e1}
.form-top-box .from-clr address{padding:10px; border-right:2px solid #333; margin:0; font-size:16px; font-weight:normal; color:black;height:98px;background: #fff;}
.form-top-box .from-clr address span{float:right;}
.form-top-box .to-clr address{padding:10px; margin:0; font-size:16px; font-weight:normal; color:black;background: #fff;}
.recipientbgcolor { background: #fff; }
.form-top-box .to-clr address span{float:right;}
.shipping_section ul{float:left; width:100%; margin:20px 0; padding:0; list-style:none;}
.shipping_section ul li{float:left; width:100%; margin:10px 0;}
.shipping_section ul li label{line-height:35px; color:#fff; font-size:16px; font-weight:normal; background-color:#1a75bb; border-radius:5px; display:block; padding:0 10px; box-shadow:0 1px 2px 0 #333; position:relative; z-index:2; margin-bottom:0;}
.shipping_section ul li label.ship_lbl{margin-top:20px;}
.pkg_box{float:left; width:100%; border-radius:5px; box-shadow: 0 1px 2px 0 #333; position:relative; z-index:0; background-color:#F0F0F0; min-height:35px; margin-left:-2px; padding:0;}
.pkg_box span{line-height:26px; font-size:16px; font-weight:600; color:#333;}
.pkg_box span.arriv{color:#9f0e09;}
.pkg_box span small{color:#1a75bb; font-size:16px; margin-right:5%;}
.delivery_pkg_box{float:left; width:100%; border-radius:5px; box-shadow: 0 1px 2px 0 #333; position:relative; z-index:0; background-color:#F0F0F0; min-height:35px; margin-left:-2px; padding:0;}

.delivery_pkg_box span{line-height:35px; font-size:16px; font-weight:600; color:#333;}
.delivery_pkg_box span.arriv{color:#9f0e09;}
.delivery_pkg_box span small{color:#1a75bb; font-size:16px;}

.pkg_box .price{float:left; background-color:#2183c2; padding:0px 0px 0px 20px; margin-right:20px;}
.fifity{height:15px; clear:both}
.shipping_section ul li div.pickadd{line-height:105px;}
.shipping_section ul li div.pickadd label{line-height:105px;}



.bartextalign { width:25%!important;	 } 
.bartextalign3 {width:75%!important; height:72px;} 
.previewshipcheck {
background: #ef1e26;
padding: 5px;
width: 23px;
height: 14px;
display:inline-block;
margin-right: 2px;
}
.previewshipcheckwhite {
background: white;
padding: 5px;
width: 23px;
height: 14px;
border: 1px solid black;
display:inline-block;
}

.pkg_boxchk{float:left; width:100%; border-radius:5px; box-shadow: 0 1px 2px 0 #333; position:relative; z-index:0; background-color:#F0F0F0; min-height:35px; margin-left:-2px; padding:0;}
.pkg_boxchk span{line-height:35px; font-size:16px; font-weight:600; color:#333;}
.pkg_boxchk span.arriv{color:#9f0e09;}
.pkg_boxchk span small{color:#1a75bb; font-size:16px; }
.pkg_boxchk .price{float:left; background-color:#2183c2; padding:0px 0px 0px 20px; margin-right:20px;}

.pickuppkg_boxchk{float:left; width:100%; border-radius:5px; box-shadow: 0 1px 2px 0 #333; position:relative; z-index:0; background-color:#F0F0F0; min-height:35px; margin-left:-2px; padding:0;}
.pickuppkg_boxchk span{line-height:35px; font-size:16px; font-weight:600; color:#333;}
.pickuppkg_boxchk span.arriv{color:#9f0e09;}
.pickuppkg_boxchk span small{color:#1a75bb; font-size:16px; }
.pickuppkg_boxchk .price{float:left; background-color:#2183c2; padding:0px 0px 0px 20px; margin-right:20px;    min-height: 72px;}
</style>
<?php include('footer.php'); ?>