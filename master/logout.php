<?php
//Project Name : Ouiship
//Developer : Prashant Gorvadia
//this page clear session of browser so user logout from system and redirect to login page
include('config.php');

session_unset(); 

// destroy the session 
session_destroy(); 

header('Location:loginsetupacct.php?msg=suclogout');

?>