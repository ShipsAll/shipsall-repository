<?php
//Project Name : Ouiship
//Developer : Prashant Gorvadia
//This file coding use for shipping rates date setting and we are skipping holidays with usage of this file
/**
 * Class Calculator
 *
 * @package BusinessDays
 */
class Calculator
{
    const MONDAY    = 1;
    const TUESDAY   = 2;
    const WEDNESDAY = 3;
    const THURSDAY  = 4;
    const FRIDAY    = 5;
    const SATURDAY  = 6;
    const SUNDAY    = 7;

    const WEEK_DAY_FORMAT = 'N';
    const HOLIDAY_FORMAT  = 'm-d';
    const FREE_DAY_FORMAT = 'Y-m-d';

    /** @var \DateTime */
    private $date;

    /** @var \DateTime[] */
    private $holidays = array();

    /** @var \DateTime[] */
    private $freeDays = array();

    /** @var int[] */
    private $freeWeekDays = array();

    /**
     * @param \DateTime $startDate Date to start calculations from
     *
     * @return $this
     */
    public function setStartDate(\DateTime $startDate)
    {
        // Use clone so parameter is not passed as a reference.
        // If not, it can brake caller method by changing $startDate parameter while changing it here.

        $this->date = clone $startDate;

        return $this;
    }

    /**
     * @param \DateTime[] $holidays Array of holidays that repeats each year. (Only month and date is used to match).
     *
     * @return $this
     */
    public function setHolidays(array $holidays)
    {
        $this->holidays = $holidays;

        return $this;
    }

    /**
     * @return \DateTime[]
     */
    private function getHolidays()
    {
        return $this->holidays;
    }

    /**
     * @param \DateTime[] $freeDays Array of free days that dose not repeat.
     *
     * @return $this
     */
    public function setFreeDays(array $freeDays)
    {
        $this->freeDays = $freeDays;

        return $this;
    }

    /**
     * @return \DateTime[]
     */
    private function getFreeDays()
    {
        return $this->freeDays;
    }

    /**
     * @param int[] $freeWeekDays Array of days of the week which are not business days.
     *
     * @return $this
     */
    public function setFreeWeekDays(array $freeWeekDays)
    {
        $this->freeWeekDays = $freeWeekDays;

        return $this;
    }

    /**
     * @return int[]
     */
    private function getFreeWeekDays()
    {
        if (count($this->freeWeekDays) >= 7) {
            throw new \InvalidArgumentException('Too many non business days provided');
        }

        return $this->freeWeekDays;
    }

    /**
     * @param int $howManyDays
     *
     * @return $this
     */
    public function addBusinessDays($howManyDays)
    {
        $iterator = 0;
        while ($iterator < $howManyDays) {
            $this->getDate()->modify('+1 day');
            if ($this->isBusinessDay($this->getDate())) {
                $iterator++;
            }
        }

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        if ($this->date === null) {
            $this->date = new \DateTime();
        }

        return $this->date;
    }

    /**
     * @param \DateTime $date
     *
     * @return bool
     */
    public function isBusinessDay(\DateTime $date)
    {
        if ($this->isFreeWeekDayDay($date)) {
            return false;
        }

        if ($this->isHoliday($date)) {
            return false;
        }

        if ($this->isFreeDay($date)) {
            return false;
        }

        return true;
    }

    /**
     * @param \DateTime $date
     *
     * @return bool
     */
    private function isFreeWeekDayDay(\DateTime $date)
    {
        $currentWeekDay = (int)$date->format(self::WEEK_DAY_FORMAT);

        if (in_array($currentWeekDay, $this->getFreeWeekDays())) {
            return true;
        }

        return false;
    }

    /**
     * @param \DateTime $date
     *
     * @return bool
     */
    private function isHoliday(\DateTime $date)
    {
        $holidayFormatValue = $date->format(self::HOLIDAY_FORMAT);
        foreach ($this->getHolidays() as $holiday) {
            if ($holidayFormatValue == $holiday->format(self::HOLIDAY_FORMAT)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param \DateTime $date
     *
     * @return bool
     */
    private function isFreeDay(\DateTime $date)
    {
        $freeDayFormatValue = $date->format(self::FREE_DAY_FORMAT);
        foreach ($this->getFreeDays() as $freeDay) {
            if ($freeDayFormatValue == $freeDay->format(self::FREE_DAY_FORMAT)) {
                return true;
            }
        }

        return false;
    }
}

class US_Federal_Holidays
{
    private $year, $list;
    const ONE_DAY = 86400; // Number of seconds in one day
 
    function __construct($year = null, $timezone = 'America/Chicago')
    {
        try
        {
            if (! date_default_timezone_set($timezone))
            {
                throw new Exception($timezone.' is not a valid timezone.');         
            }
 
            $this->year = (is_null($year))? (int) date("Y") : (int) $year;
            if (! is_int($this->year) || $this->year < 1997)
            {
                throw new Exception($year.' is not a valid year. Valid values are integers greater than 1996.');
            }
         
            $this->set_list();
        }
 
        catch(Exception $e)
        {
            echo $e->getMessage();
            exit();
        }
    }
 
    private function adjust_fixed_holiday($timestamp)
    {
        $weekday = date("w", $timestamp);
        if ($weekday == 0)
        {
            return $timestamp + self::ONE_DAY;
        }
        if ($weekday == 6)
        {
            return $timestamp - self::ONE_DAY;
        }
        return $timestamp;
    }
 
    private function set_list()
    {
        $this->list = array
        (
            array
            (
                "name" => "New Year's Day", 
                        // January 1st, if not Saturday/Sunday
                "timestamp" => $this->adjust_fixed_holiday(mktime(0, 0, 0, 1, 1, $this->year))
                ), 
            array
            (
                "name" => "Birthday of Martin Luther King, Jr.", 
                        // 3rd Monday of January
                "timestamp" => strtotime("3 Mondays", mktime(0, 0, 0, 1, 1, $this->year))
                ),
            array
            (
                "name" => "Wasthington's Birthday", 
                        // 3rd Monday of February
                "timestamp" => strtotime("3 Mondays", mktime(0, 0, 0, 2, 1, $this->year))
                ),
            array
            (
                "name" => "Memorial Day ", 
                        // last Monday of May
                "timestamp" => strtotime("last Monday of May $this->year")
                ),
            array
            (
                "name" => "Independence day ", 
                        // July 4, if not Saturday/Sunday
                "timestamp" => $this->adjust_fixed_holiday(mktime(0, 0, 0, 7, 4, $this->year))
                ),
            array
            (
                "name" => "Labor Day ", 
                        // 1st Monday of September
                "timestamp" => strtotime("first Monday of September $this->year")
                ),
            array
            (
                "name" => "Columbus Day ", 
                        // 2nd Monday of October
                "timestamp" => strtotime("2 Mondays", mktime(0, 0, 0, 10, 1, $this->year))
                ),
            array
            (
                "name" => "Veteran's Day ",
                        // November 11, if not Saturday/Sunday
                "timestamp" => $this->adjust_fixed_holiday(mktime(0, 0, 0, 11, 11, $this->year))
                ),  
            array
            (
                "name" => "Thanksgiving Day ", 
                        // 4th Thursday of November
                "timestamp" => strtotime("4 Thursdays", mktime(0, 0, 0, 11, 1, $this->year))
                ),
            array
            (
                "name" => "Christmas ", 
                        // December 25 every year, if not Saturday/Sunday
                "timestamp" => $this->adjust_fixed_holiday(mktime(0, 0, 0, 12, 25, $this->year))
            )
        );
    }
 
    public function get_list()
    {
        return $this->list;
    }
 
    public function is_holiday($timestamp)
    {
        foreach ($this->list as $holiday)
        {
           if ($timestamp == $holiday["timestamp"]) return true;
        }
     
        return false;
    }
}