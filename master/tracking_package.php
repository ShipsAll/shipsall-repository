<?php
//Project Name : Ouiship
//Developer : Prashant Gorvadia
//Trackign page use for track package of shipment package there are some things remain because that created by another developer
include('config.php');
include('header.php');
?>
<!-- track page css code coems here : start ->
<style>
@charset "utf-8";
/* CSS Document */

.CSSTableGenerator {
margin:0px;padding:0px;
width:100%;
box-shadow: 10px 10px 5px #888888;
border:1px solid #ffffff;

-moz-border-radius-bottomleft:0px;
-webkit-border-bottom-left-radius:0px;
border-bottom-left-radius:0px;

-moz-border-radius-bottomright:0px;
-webkit-border-bottom-right-radius:0px;
border-bottom-right-radius:0px;

-moz-border-radius-topright:0px;
-webkit-border-top-right-radius:0px;
border-top-right-radius:0px;

-moz-border-radius-topleft:0px;
-webkit-border-top-left-radius:0px;
border-top-left-radius:0px;
}.CSSTableGenerator table{
border-collapse: collapse;
border-spacing: 0;
width:100%;
height:100%;
margin:0px;padding:0px;
}.CSSTableGenerator tr:last-child td:last-child {
-moz-border-radius-bottomright:0px;
-webkit-border-bottom-right-radius:0px;
border-bottom-right-radius:0px;
}
.CSSTableGenerator table tr:first-child td:first-child {
-moz-border-radius-topleft:0px;
-webkit-border-top-left-radius:0px;
border-top-left-radius:0px;
}
.CSSTableGenerator table tr:first-child td:last-child {
-moz-border-radius-topright:0px;
-webkit-border-top-right-radius:0px;
border-top-right-radius:0px;
}.CSSTableGenerator tr:last-child td:first-child{
-moz-border-radius-bottomleft:0px;
-webkit-border-bottom-left-radius:0px;
border-bottom-left-radius:0px;
}.CSSTableGenerator tr:hover td{
background-color:#d3e9ff;


}
.CSSTableGenerator td{
vertical-align:middle;

background-color:#aad4ff;

border:1px solid #ffffff;
border-width:0px 1px 1px 0px;
text-align:left;
padding:7px;
font-size:10px;
font-family:Arial;
font-weight:normal;
color:#160404;
}.CSSTableGenerator tr:last-child td{
border-width:0px 1px 0px 0px;
}.CSSTableGenerator tr td:last-child{
border-width:0px 0px 1px 0px;
}.CSSTableGenerator tr:last-child td:last-child{
border-width:0px 0px 0px 0px;
}
.CSSTableGenerator tr:first-child td{
background:-o-linear-gradient(bottom, #0057af 5%, #0057af 100%);	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #0057af), color-stop(1, #0057af) );
background:-moz-linear-gradient( center top, #0057af 5%, #0057af 100% );
filter:progid:DXImageTransform.Microsoft.gradient(startColorstr="#0057af", endColorstr="#0057af");	background: -o-linear-gradient(top,#0057af,0057af);

background-color:#0057af;
border:0px solid #ffffff;
text-align:center;
border-width:0px 0px 1px 1px;
font-size:14px;
font-family:Arial;
font-weight:bold;
color:#ffffff;
}
.CSSTableGenerator tr:first-child:hover td{
background:-o-linear-gradient(bottom, #0057af 5%, #0057af 100%);	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #0057af), color-stop(1, #0057af) );
background:-moz-linear-gradient( center top, #0057af 5%, #0057af 100% );
filter:progid:DXImageTransform.Microsoft.gradient(startColorstr="#0057af", endColorstr="#0057af");	background: -o-linear-gradient(top,#0057af,0057af);

background-color:#0057af;
}
.CSSTableGenerator tr:first-child td:first-child{
border-width:0px 0px 1px 0px;
}
.CSSTableGenerator tr:first-child td:last-child{
border-width:0px 0px 1px 1px;
}




</style>
<!-- track page css code coems here : end ->
<body>
<!-- topbar starts -->
<?php include('header-menu.php'); ?>
<!-- topbar ends -->
<div class="ch-container" style="text-align:center;">
<h3>Track Package</h3>
<?php if($_REQUEST['tracking_code']!='') { ?>
<div id="overlay" style="margin-right: 45%;margin-left: 45%; ">
<img src="images/ajax-loader-7.gif" title="Shipping Rates Fetching"><strong></strong>
</div>
<?php } ?>
</div>
    <div class="ch-container" style="margin-left:15%;text-align:center;"> <Br>
    <div class="box col-md-10">
    <div class="main">
   	 <div class="row">
    <?php if(isset($_GET['msg']) && $_GET['msg']=='invtrkdetail') { ?>
    <div class="alert alert-success">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong>Please Try Again With Different Number!</strong> The tracking number does not belong to the carrier you specified. Please confirm that both the carrier and tracking number are correct. </div>
    </div>
    <?php } elseif(isset($_GET['msg']) && $_GET['msg']=='invalid') { ?>
   	 <div class="alert alert-success">
    	<button type="button" class="close" data-dismiss="alert">&times;</button>
    		<strong>Please Try Again !</strong> Tracking Number and Carrier Both Requr. </div>
   	 </div>
    <?php } ?>
    </div> 
        <div class="main"><br /><br />
        <div>
        <form method="post" name="tracking_form" enctype="multipart/form-data" action="tracking_package.php" onSubmit="myFunction()">
            <input type="hidden" name="tracking_hidden" value="1">
            <p style="color:black">Tracking Number</p>
            <div><textarea name="tracking_code" cols="45" rows="1" placeholder="Please Enter Tracking Number"  required></textarea></div><br>
            <div>
            <select name="tracking_carrier" id="tracking_carrier">
                <option value="">Select Carrier</option>
                <option value="FedEx">FedEx</option>
                <option value="USPS">USPS</option>
                <option value="UPS">UPS</option>
                <option value="DHL">DHL</option>
            </select>	      
            </div><br>
            <div style="margin-right:0%;"><input type="submit" name="image" src="images/button.png" value="Submit" class="btn btn-primary"></div>
        </form>
        
        <div style="height:50px;"></div>                         
        </div>
        <?php
        if($_REQUEST['tracking_code']!=''){
            //Easypost Api code : Start
        require_once("lib/easypost.php");
        //Below API function for fetching api code from database : Start
        $fetchapikey = getapikey(); 
        //Below API function for fetching api code from database : End
        \EasyPost\EasyPost::setApiKey($fetchapikey);
        
        $tracking_code = $_REQUEST['tracking_code'];
        $tracking_carrier = $_REQUEST['tracking_carrier'];
        $tracker = \EasyPost\Tracker::create(array('tracking_code' => $tracking_code, 'carrier' => $tracking_carrier));
        //Easypost Api code : End
        ?>
        <?php if($tracker['status']!=''){ ?>
        <div class="CSSTableGenerator">
        <table><tr><td>tracking_code</td><td>status</td><td>carrier</td><td>Weight</td><td>Estimated Delivery Date</td><td>Created Date</td><td>Updated Date</td></tr><?php echo "<tr><td>$tracker[tracking_code]</td><td>$tracker[status]</td><td>$tracker[carrier]</td><td>$tracker[weight]</td><td>$tracker[est_delivery_date]</td><td>$tracker[created_at]</td><td>$tracker[updated_at]</td></tr>"; ?>
        </table>
        </div>
        <div class="CSSTableGenerator">
            <table>
                <tr><td>Message</td><td>City</td><td>State</td><td>Country</td><td>Zip</td><td>status</td><td>Date/Time</td></tr>
                <?php
                foreach($tracker['tracking_details'] as $value) {
                ?><tr><td><?php print $value['message']; ?></td><td><?php print $value['tracking_location']['city']; ?></td><td><?php print $value['tracking_location']['state']; ?></td><td><?php print $value['tracking_location']['country']; ?></td><td><?php print $value['tracking_location']['zip']; ?></td><td><?php print $value['status']; ?></td><td><?php print $value['datetime']; ?></td></tr>
                <?php } ?>
            </table>
        </div>
        <?php } else {  ?>
        <META http-equiv="refresh" content="0;URL=http://www.ouiship.com/tracking_package.php?msg=invtrkdetail">
        <?php
        exit;
        } 
        }
        ?>
        </div>
    </div>
</div>
</div>
</div>
</div>
<br><br><br><br><br><br>
<?php include('footer.php'); ?>


